import os
from jinja2 import Environment, FileSystemLoader
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

modelname = 'V0.13' #snakemake.params.modelname
path_template = "runfile_lhmfresh_steadystate.template"
path_runfile = Path(f"data/4-output/lhmfresh_v{modelname}")
path_lhmfresh = "data/1-external/" #Path(snakemake.params.path_lhmfresh)#"e:/LHM_4.1/Data/modflow")
#path_lhm_exe = 
path_rch = "data/1-external/rch/groundwater_recharge_average_2011-2018_md.idf"#snakemake.input.path_rch  # to be filled in, not a regular part of LHM fresh

lhmfresh_paths = {
    "path_ibound": path_lhmfresh + "bnd/IBOUND",
    "path_shd": path_lhmfresh + "shd/HEAD_STEADY-STATE",
    "path_kdw": path_lhmfresh + "kd/MDL_KD",
    "path_vcw": path_lhmfresh + "c/MDL_C",
    "path_top": path_lhmfresh + "top/MDL_TOP",
    "path_bot": path_lhmfresh + "bot/MDL_BOT",
    "path_anifct": path_lhmfresh + "ani/fct_laag",
    "path_anihk": path_lhmfresh + "ani/hk_laag",
    "path_hfb": path_lhmfresh + "hfb/hfb",
    "path_wel": path_lhmfresh + "wel",
    "path_drn": path_lhmfresh + "drn",
    "path_riv": path_lhmfresh + "riv",
    "path_ghb": path_lhmfresh + "ghb",
    "path_rch": path_rch,
}

path_runfile.mkdir(exist_ok=True, parents=True)
# make paths relative to runpath
lhmfresh_paths = {k:Path(os.path.relpath(p, path_runfile)) for k,p in lhmfresh_paths.items()}


env = Environment(loader=FileSystemLoader("config"))
template = env.get_template(path_template)
with open(path_runfile / f"lhmfresh_{modelname}.run", "w") as f:
    f.write(template.render(**lhmfresh_paths))

# and run LHM

