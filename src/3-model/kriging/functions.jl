using RCall
using LibPQ, Tables
using DataFrames
using NetCDF
using StatsFuns
using Statistics
using CSV
using DataFramesMeta

"""
  value: The observation value
  pred: The predicted value
  return: MAE (1), RMSE (2), COR(3) & ME(4)

    1) Mean Absolute Error (MAE)
    2) Root Mean Squared Error (RMSE)
    3) Pearson's Correlation (COR)
    4) Mean Error (ME)
"""
function crossstats(value, pred)
   # Calculate RMSE
  MAE = mean(abs.(value - pred))
   # Calculate Mean absolute error
  RMSE = sqrt(mean((value - pred) .^ 2))
   # calculate correlation
  COR = cor(value, pred)
   # Mean difference
  ME = mean(abs.(value - pred))
  return MAE, RMSE, COR, ME
end

"""
Finds the nearest value to x in an sorted array
  a: Sorted array
  x: value
  return: index of nearest value to x in array a
"""
function searchsortednearest(a, x)
  idx = searchsortedfirst(a, x)
  if (idx == 1)
    return idx
  end
  if (idx > length(a))
    return length(a)
  end
  if (a[idx] == x)
    return idx
  end
  if (abs(a[idx] - x) < abs(a[idx-1] - x))
    return idx
  else
    return idx - 1
  end
end

"""
Finds the nearest predicted value for observation i in Data
   Chloride: 3d Array of predicted chloride concentrations
   Data: DataFrame of Data with x, y, z values
   xcoord: vector of unique x-coordinates*
   ycoord: vector of unique y-coordinates*
   Z: vector of unique z-coordinates*
   return: Dataframe with corresponding predicted chloride values
* = optional
"""
function find_nearest_predicted_value(
  Chloride,
  Data,
  xcoord = nothing,
  ycoord = nothing,
  Z = nothing,
)
   # create absolute error column
  Data[:pred_value] = NaN

  if all([xcoord, ycoord, Z] .== nothing)
      # Create 3 vectors for the coordinates
    xcoord = collect(125:250:300000)
    ycoord = collect(300125:250:625000)
    Z = reverse(Int64.(vcat(
      110:-30:50,
      40:-10:10,
      5,
      0,
      -(2:2:20),
      -(25:5:50),
      -(60:10:200),
      -(220:20:300),
      -500,
    )))
  end

  Threads.@threads for r = 1:nrow(Data)
    i = searchsortednearest(xcoord, Data[r, :x])
    j = searchsortednearest(ycoord, Data[r, :y])
    k = searchsortednearest(Z, Data[r, :z])

      # Calculate error for each data observation
    Data[r, :pred_value] = Chloride[k, j, i]
  end

  return Data
end

"""
Calculate the indicator values for the freshem data
    df: Freshem data
    indicators: the indicator threshold
    return: Dataframe with x,y,z,indicator values
"""
function indicators_freshem(df, indicators)
    # Add the indicators as columns to the df
  df[:,[:I_150, :I_500, :I_1000, :I_3000, :I_5000, :I_10000, :I_15000]] = 0.0

    # Set the percentages present in the freshem data
  perc = [10, 25, 40, 50, 60, 75, 90]

  Threads.@threads for i = 1:length(indicators)
    for j = 1:nrow(df)
      chloride_value = df[
        j,
        [:Cl_p10, :Cl_p25, :Cl_p40, :Cl_p50, :Cl_p60, :Cl_p75, :Cl_p90],
      ]
      chloride_value = Vector(chloride_value)

            # Loop over each row and calculate the indicator value for each indicator
      if indicators[i] < extrema(chloride_value)[1]
        indicator_value = 0.0
      elseif indicators[i] > extrema(chloride_value)[2]
        indicator_value = 1.0
      elseif indicators[i] == extrema(chloride_value)[2]
        indicator_value = 0.90
      else
        index_j = findfirst(>(indicators[i]), chloride_value)
        position = (indicators[i] - chloride_value[index_j-1]) /
                   (chloride_value[index_j] - chloride_value[index_j-1])
        indicator_value = (perc[index_j-1] +
                           position * (perc[index_j] - perc[index_j-1])) / 100
      end

            # write results back in the df
      df[j, Symbol("I_$(indicators[i])")] = indicator_value
    end
  end

    # Remove redundant data
  select!(df, Not([:Cl_p10, :Cl_p25, :Cl_p40, :Cl_p50, :Cl_p60, :Cl_p75, :Cl_p90]) )
  return df
end

"""
Using NetCDF to right a 4 dimensional grid to a NetCDF
    array_x: 4 dimensional array of indicator values or chloride concentrations
    array_y: 1 dimensional array of indicators or percentages
    filename: The name of the output file
    Return: 4 dimensional netcdf
"""
function write_netcdf_4dims(array_x, array_y, filename)
    # Define x-coord, y-coord and depth
  xcoord = collect(125:250:300000)
  ycoord = collect(300125:250:625000)
  Z = reverse(Int64.(vcat(
    110:-30:50,
    40:-10:10,
    5,
    0,
    -(2:2:20),
    -(25:5:50),
    -(60:10:200),
    -(220:20:300),
    -500,
  )))
  grid_n = ["I_$i" for i in array_y]

  xatts = Dict(
    "longname" => "The netcdf consist of 3D grids for $filename $array_y.
          z-coordinates begin from low (-500 m NAP) to high (110 m NAP).
          For example Array[1,1,:,:] grabs the first 3D model for height -500 m
          and Array[1,46,:,:] grabs the first 3D model for height 110 m",
    "units" => "-",
  )
  xcoordatts = Dict(
    "longname" => "x-coordinates in EPSG:28992",
    "units" => "meters",
  )
  ycoordatts = Dict(
    "longname" => "y-coordinates in EPSG:28992",
    "units" => "meters",
  )
  Zatts = Dict(
    "longname" => "z-coordinates in EPSG:28992",
    "units" => "meters NAP",
  )
  grid_n_atts = Dict(
    "longname" => "$filename dimensions $array_y",
    "units" => "-",
  )

  output_filename = "$(filename)_" * replace(
    strip(string(permutedims(array_y)), ['[', ']']),
    " " => "_",
  )
  fn = joinpath(@__DIR__, "$(output_filename).nc")
  isfile(fn) && rm(fn)
  nccreate(
    fn,
    "$filename",
    "$filename dimension",
    grid_n,
    grid_n_atts,
    "z-coordinates",
    Z,
    Zatts,
    "y-coordinates",
    ycoord,
    ycoordatts,
    "x-coordinates",
    xcoord,
    xcoordatts,
    atts = xatts,
  )

  ncwrite(array_x, fn, "$filename")
end

"""
Using NetCDF to right a 3 dimensional grid to a NetCDF
    array_x: 3 dimensional array of indicator values or chloride concentrations
    array_y: 1 dimensional array of indicators or percentages
    filename: The name of the output file
    Return: 3 dimensional netcdf
"""
function write_netcdf_3dims(array_x, varname, filename)
    # Define x-coord, y-coord and depth
  xcoord = collect(125:250:300000)
  ycoord = collect(300125:250:625000)
  Z = reverse(Int64.(vcat(
    110:-30:50,
    40:-10:10,
    5,
    0,
    -(2:2:20),
    -(25:5:50),
    -(60:10:200),
    -(220:20:300),
    -500,
  )))

  xatts = Dict(
    "longname" => "The netcdf consist of 3D grids for $varname.
          z-coordinates begin from low (-500 m NAP) to high (110 m NAP).
          For example Array[1,1,:,:] grabs the first 3D model for height -500 m
          and Array[1,46,:,:] grabs the first 3D model for height 110 m",
    "units" => "-",
  )
  xcoordatts = Dict(
    "longname" => "x-coordinates in EPSG:28992",
    "units" => "meters",
  )
  ycoordatts = Dict(
    "longname" => "y-coordinates in EPSG:28992",
    "units" => "meters",
  )
  Zatts = Dict(
    "longname" => "z-coordinates in EPSG:28992",
    "units" => "meters NAP",
  )

  fn = joinpath(@__DIR__, "$filename.nc")
  isfile(fn) && rm(fn)
  nccreate(
    fn,
    "indicator_$varname",
    "z-coordinates",
    Z,
    Zatts,
    "y-coordinates",
    ycoord,
    ycoordatts,
    "x-coordinates",
    xcoord,
    xcoordatts,
    atts = xatts,
  )

  ncwrite(array_x, fn, "indicator_$varname")
end

"""
Using Gstat in R to perform Indicator kriging
    Data: dataframe with x, y, z & indicators
    nugget: nugget value
    sill: sill value
    range: range value
    anis: anisotropy
    varname: name of the indicator column
    Return: 3D grid with mean & var values
"""
function Kriging_gstat(nugget, sill, range, anis, cores, varname)
  dir = @__DIR__
  @rput nugget sill range anis varname cores dir
  R"""
    library(raster)
    library(rgdal)
    library(parallel)
    library(gstat)
    library(sp)

    # Set work directory to script folder
    setwd(dir)

    # Increase the memory limit to 20 GB
    memory.limit(size = 20000)

    # Create Spheric Variogram model with predefined parameters (psill, range and nugget)
    m <-
      vgm(
        psill = sill - nugget,
        model = "Sph",
        range = range,
        nugget = nugget
      )

    # Multiple indicator kriging (parallel)
    no_cores <- cores

    filenames <- list.files("grids/")
    filenames <- paste0("grids/",filenames)

    # Initiate cluster
    cl <- makeCluster(no_cores)

    clusterExport(
      cl = cl,
      varlist = c("m","varname","filenames"),
      envir = .GlobalEnv
    )
    clusterEvalQ(cl = cl, expr = c(library('sp'), library('gstat'),library('raster')))

    function_kriging <- function(X) {
      load(filenames(X))

      eval(parse(
        text = paste0(
          "krige(",
          varname,
          "~1, df, grid, m, nmax=50, nmin=40, omax=7, maxdist =36000, force=TRUE)"
        )
      ))
    }

    print("start kriging")
    system.time(parallelX <- parLapplyLB(cl = cl, X = 1:length(filenames), function_kriging))
    print("kriging done")

    print(stopCluster(cl))

    print("merge the kriging results")
    system.time( df <-  do.call(rbind,lapply(parallelX, data.frame)))
    rm(parallelX)
    """
  @rget df
  select!(df,Not([:optional]))
  println("transfered the data to Julia")

    # Convert the z coordinates back to original values
  df.z = df.z / anis

   # read spatialpoint outside kriging extent
  df_out = Feather.read(joinpath(@__DIR__, "df_out_extent.feather"))
  rename!(df_out, Symbol("var1.pred") => :var1_pred, Symbol("var1.var") => :var1_var)
  df_out.var1_pred = NaN
  df_out.var1_var = NaN

    # merge krigsing results with cells outside extent
  df = vcat(df, df_out)

    # Sort the coluns on x, y, x and reshape them into a 3D grid
  println("sorting...")
  df = sort!(df, [:x, :y, :z], rev = false)
  df = reshape(df.var1_pred, (46, 1300, 1200))
  println("sorting done")

    # Remove R variable from memory
  R"rm(list=ls())"

  return df
end

"""
Uses Gstat in R to make an omnidirectional variogram & calculate anisotropy
Data: Is a dataframe with your data with x,y,z and the name of the column containing the values

return:
    nugget
    sill
    range
    anisotropy
    *figures when True
"""
function R_variogram(Data, varname, figures::Bool, path)
  cd(path)
  @rput varname Data figures
  @time R"""
      # Loading required libraries
      library(gstat)
      library(sp)

      # Changing the Dataset into a SpatialPointsDataFrame
      coordinates(Data) = c("x", "y", "z")

      # Set the coordinate system to RD new
      proj4string(Data) = CRS("+init=epsg:28992")

      # Horizontal variability
      print("calculating Horizontal variability")
      eval(parse(text = paste0("Data.vgm.xy <- variogram(",varname,"~z, Data, cutoff = 15000, width = 100, dX = 1)")))
      Data.fit.xy <- fit.variogram(Data.vgm.xy, vgm(0, "Sph", 5000, 0))
      Horizontal_name = paste0("Horizontal_variability_", varname,".RData")
      save(Data.vgm.xy, file=Horizontal_name)

      if (figures){
          png(file=paste0("Horizontal variability ", varname, ".png"),width=1600,height=900,res=100)
          print(plot(Data.vgm.xy, Data.fit.xy, ylab = paste0('Semivariance Indicator ', varname), xlab = 'horizontal distance (m)'))
          dev.off()
      }

      # vertical variability
      print("calculating vertical variability")
      eval(parse(text = paste0("Data.vgm.z <- variogram(",varname,"~ x + y, Data, cutoff = 100, width = 1, dX = 0)")))
      Data.fit.z <- fit.variogram(Data.vgm.z, vgm(0.01, "Sph", 100, 0.01))
      vertical_name = paste0("Vertical_variability_", varname,".RData")
      save(Data.vgm.z , file=vertical_name)

      if (figures){
          png(file=paste0("Vertical variability ", varname, ".png"),width=1600,height=900,res=100)
          print(plot(Data.vgm.z, Data.fit.z, ylab = paste0('Semivariance Indicator ', varname), xlab = 'horizontal distance (m)'))
          dev.off()
      }

      # Calculate anisotrophy
      anisotropy <- Data.fit.xy$range[2]/Data.fit.z$range[2]

      # Add anisotrophy to the z-coordinates
      Data<- as(Data,'data.frame')
      Data$z <- Data$z * anisotropy

      # Changing the Dataset into a SpatialPointsDataFrame
      coordinates(Data) = c("x", "y", "z")

      # Set the coordinate system to RD new
      proj4string(Data) = CRS("+init=epsg:28992")

      # variogram
      print("calculating variogram")
      eval(parse(text = paste0("Data.vgm.xyz <- variogram(",varname,"~1, Data, cutoff = 15000, width = 100, dX = 1)")))
      Data.fit.xyz = fit.variogram(Data.vgm.xyz, vgm(0,"Sph",5000,0))
      variogram_name = paste0("Variogram_variability_", varname,".RData")
      save(Data.vgm.xyz , file=variogram_name)

      if (figures){
          png(file=paste0("variogram.png ", varname, ".png"), width=1600, height=900, res=100)
          print(plot(Data.vgm.xyz, Data.fit.xyz, ylab = paste0('Semivariance Indicator ', varname), xlab = 'horizontal distance (m)'))
          dev.off()
      }

      nugget <- Data.fit.xyz$psill[1]
      sill <- Data.fit.xyz$psill[2] + Data.fit.xyz$psill[1]
      range <- Data.fit.xyz$range[2]
  """
  @rget nugget
  @rget sill
  @rget range
  @rget anisotropy

  df = DataFrame(
    varname = varname,
    nugget = nugget,
    sill = sill,
    range = range,
    anisotropy = anisotropy,
  )

  return df
end


"""
Read data from PostGIS and return a Dataframe
user: Your PostGIS username
password: Your password to the PostGIS
"""
function read_postgis(user, password)
    # Set up connection to PostgreSQL
  conn = LibPQ.Connection("dbname=zoetzout user=$user host= tl-pg043.xtr.deltares.nl port = 5432 password = $password")
  
    # Select different views from a POSTGIS connection.
  result_softdata = execute(
    conn,
    "
  SELECT x,y,z,value *1000 as value,
  CASE WHEN typecode = 'SOFT_SEA' THEN 1440000
      WHEN typecode = 'SOFT_DEEP' THEN 225000000
      WHEN typecode = 'SOFT_FRESHBRACK' THEN 1440000
      WHEN typecode = 'SOFT_BRACKSALT' THEN 1440000
      WHEN typecode = 'SOFT_TRANSGR' THEN 225000000
      WHEN typecode = 'SOFT_CONNATE' THEN 225000000
      END AS variance
  FROM xyzv_softdata
  ",
    )
    result_geophysics = execute(
      conn,
      "
    SELECT locationid,locationcode,typecode, x,y,z,value,
    CASE WHEN year < 2000 THEN (1 + (2000- year) / 10 ) * variance
    else variance
    END AS variance
    FROM (Select locationid,locationcode,typecode,x,y,z,datetime,value * 1000 as value, extract(year from datetime) as year,
          CASE WHEN typecode = 'BL' THEN 1440000
          WHEN typecode = 'AEM' THEN 25000000
          WHEN typecode = 'ECPT' THEN 1440000
          WHEN typecode = 'VES' THEN 225000000
          WHEN typecode = 'TEC' THEN 1440000
          WHEN typecode = 'SF' THEN 1440000
          END AS variance
     FROM xyzv_geophysics
     where (locationid not in (233427,233431,233432,233433,234018,234023,234281,234380,
             234385,234574,234604,234727,234738,234869,235165,235220,235404,235430,235438,235453,
             235455,235456,235478,235495,235504,235511,235537,235548,235909,235929,235930,235943,
             237921,238188,244842)
    		or locationcode not in ('B12C0065_00','B12D0238_00','B12D0239_00','B12D0241_00',
            'B28A0058_02','B28B0057_02','B32E0066_02','B34A0097_00','B34C0086_00',
            'B39D0153_02','B39E0085_02','B40F2041_01','B41E0256_00','B44D0457_00',
            'B50A0340_00','B50E0371_00','B57E0082_00','B58A0130_00','B58B0154_01',
            'B58D0463_00','B58D0478_00','B58D0480_00','B58G0191_00','B60B0105_00',
            'B60C0788_03','B60C0903_02','B61F0323_00','B62B0838_00','60A1827','62A2925',
            '39H3313','B50F0429','W12D0074','W12G0026','W52G0005')
    		or typecode not in ('BL','BL','BL','BL','BL','BL','BL','BL','BL','BL','BL','BL',
            'BL','BL','BL','BL','BL','BL','BL','BL','BL','BL','BL','BL','BL','BL','BL',
            'BL','BL','BL','BL','BL','VES','VES','VES')
    		or value not in (18,18,18,18,18,14.13,18,18,17.093,13.643,18,18,18,12.984,10.765,
        18,18,18,18,13.74,18,18,18,13.489,14.234,18,16.351,18,10.866,11.888,18,18,18,18,18))
     ) as subquery;
    ",
    )
    result_analyses_gw = execute(
      conn,
      "
    SELECT x,y,z,value,
    CASE WHEN year < 2000 THEN (1 + (2000- year) / 10 ) * variance_correct
    else variance_correct
    END AS variance
    FROM
      (Select x,y,z, value, year, variance,
      			CASE WHEN variance < 40000 THEN 40000
      			When variance is null THEN 40000
      			else variance
      			END AS variance_correct
      FROM
  		(SELECT x, y, z, median_value * 1000 as value, Power((stddev_value*1000),2) as variance, count, end_time, extract(year from end_time) as year
  		FROM xyzv_analyses_gw_aggregate) as subquery1) as subquery2;
    ",
    )

    # Transfer the data to a columntable.
  data_softdata = columntable(result_softdata)
  data_geophysics = columntable(result_geophysics)
  data_analyses_gw = columntable(result_analyses_gw)

    # Merge everything into a dataframe
    data_softdata = DataFrame(
      [
       data_softdata.x,
       data_softdata.y,
       data_softdata.z,
       Float64.(data_softdata.value),
       data_softdata.variance,
       [3 for i = 1:length(data_softdata.x)],
      ],
      [:x, :y, :z, :value, :variance, :type],
    )
    data_geophysics = DataFrame(
      [
       data_geophysics.x,
       data_geophysics.y,
       data_geophysics.z,
       Float64.(data_geophysics.value),
       data_geophysics.variance,
       [2 for i = 1:length(data_geophysics.x)],
      ],
      [:x, :y, :z, :value, :variance, :type],
    )
    data_analyses_gw = DataFrame(
      [
       data_analyses_gw.x,
       data_analyses_gw.y,
       data_analyses_gw.z,
       Float64.(data_analyses_gw.value),
       data_analyses_gw.variance,
       [1 for i = 1:length(data_analyses_gw.x)],
      ],
      [:x, :y, :z, :value, :variance, :type],
    )

  df = vcat(data_geophysics, data_analyses_gw, data_softdata)

  # Filter the duplicates
  df = by(df, [:x, :y, :z], [:value => first, :variance => first, :type => first])
  rename!(df, :value_first => :value, :variance_first => :variance, :type_first => :type)

  # replace NaN with missing
  df.x[isnan.(df.x)] .= missing
  df.y[isnan.(df.y)] .= missing
  df.z[isnan.(df.z)] .= missing

  # Drop all the missing values
  df = dropmissing(df, [:x, :y, :z, :value])

  # Close the POSTGIS Connection
  close(conn)

  # return the DataFrame
  return df
end

"""
Read freshem data from PostGIS and return a Dataframe
user: Your PostGIS username
password: Your password to the PostGIS
"""
function load_freshem(user, password)
  conn = LibPQ.Connection("dbname=zoetzout user=$user host= tl-pg043.xtr.deltares.nl port = 5432 password = $password")

  # Select from POSTGIS connection.
  Data = execute(
    conn,
    "
    SELECT l.x, l.y, d.z, p.parametercode, d.value * 1000 as value
    FROM dataseries d
    join location l on l.locationid = d.locationid
    join parameter p on p.parameterid = d.parameterid
    where p.parameterid > 4
    order by l.x;
",
  )

  df = columntable(Data)

  # Merge everything into a dataframe
  df = DataFrame(
    [df.x, df.y, df.z, df.parametercode, df.value],
    [:x, :y, :z, :parametercode, :value],
  )

  df = unstack(df, :parametercode, :value)
  df = df[completecases(df), :]

  return df
end

"""
Function to derive indicator values at given indicators for a set of values.
DataValues: input vector with values.
Indicators: vector of the set indicator thresholds.
Variance: variances around the mean values *optional input* .
"""
function set_indicators(
  data_values,
  indicators,
  variance = nothing,
  logscale = false,
)
  df = DataFrame()

  for i in indicators
    if variance == nothing
      df[Symbol("I_$i")] = float(data_values .<= i)
    else
      if logscale
        df[Symbol("I_$i")] = cdf.(
          LogNormal.(
            log.(data_values .^ 2 ./ sqrt.(variance .+ data_values .^ 2)),
            sqrt.(log.(1 .+ variance ./ data_values .^ 2)),
          ),
          i,
        )
      else
        df[:,Symbol("I_$i")] = normcdf.(data_values, sqrt.(variance), i)
      end
    end
        # correct values above 1.
    clamp.(df[:,Symbol("I_$i")], 0, 1.0)
  end

  return df
end

"""
Perform order relations correction for each threshold and calculate the
corresponding chloride concentration for percentile i.
  indicators_mean: The cumulative distribution function for all thresholds.
  indicators: A vector for all the used indicator thresholds.
  domain: Dimension of the indicators_mean
  perc: A vector of percentiles.
  return: Grid with chloride values for x amount of percentile.
"""
function ordrel(indicators_mean, indicators, domain, perc)
    # initial
  ni, nj, nk = domain
  output_grid = zeros(length(perc), domain...)
  dim_ind = vcat(0, indicators, 18000)
  indicators_mean = vcat(
    zeros(1, domain...),
    indicators_mean,
    ones(1, domain...),
  )

  Threads.@threads for i = 1:ni
    for j = 1:nj, k = 1:nk
      if all(indicators_mean[2:end-1, i, j, k] .== -999.0)
        for p = 1:length(perc)
          output_grid[p, i, j, k] = NaN
        end
      else
            # The cdf can never be lower than 0 or higher than 1.
        corr_ind_mean0 = clamp.(indicators_mean[:, i, j, k], 0, 1)

            # make two coppies of the cdf for the order relations
        corr_ind_mean1 = copy(corr_ind_mean0)
        corr_ind_mean2 = copy(corr_ind_mean0)

            # Correct sequentially up, then down, and then average:
        for x = 2:length(dim_ind)
          if corr_ind_mean1[x] < corr_ind_mean1[x-1]
            corr_ind_mean1[x] = corr_ind_mean1[x-1]
          end
        end
        for y = (length(dim_ind) - 2):-1:1
          if corr_ind_mean2[y] > corr_ind_mean2[y+1]
            corr_ind_mean2[y] = corr_ind_mean2[y+1]
          end
        end
        for z = 1:length(dim_ind)
          corr_ind_mean0[z] = 0.5 * (corr_ind_mean1[z] + corr_ind_mean2[z])
        end

            # Calculate at every precentile the chloride concentration.
        for p = 1:length(perc)
          percentile = perc[p] / 100
          index_i = findfirst(>=(percentile), corr_ind_mean0)
          position = (percentile - corr_ind_mean0[index_i-1]) /
                     (corr_ind_mean0[index_i] - corr_ind_mean0[index_i-1])
          cl_value = dim_ind[index_i-1] +
                     position * (dim_ind[index_i] - dim_ind[index_i-1])
          output_grid[p, i, j, k] = cl_value
        end
      end
    end
  end
  return output_grid
end
