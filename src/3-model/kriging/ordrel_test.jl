using Plots
using Test

include("functions.jl")

# Set indicator thresholds
indicators = [100, 200, 300, 500]

domain = [2,2,2]

# Make test data for a 2x2x2 grid for 5 dims
test_data = zeros(length(indicators), domain...)
test_data[:, 1, 1, 1] = [0.1, 0.3, 0.55, 0.90]
test_data[:, 1, 2, 1] = [0.4, 0.3, 0.50, 1.2]
test_data[:, 2, 1, 1] = [0.5, 0.5, 0.7, 1.0]
test_data[:, 2, 2, 1] = [0.6, 0.8, 1.0, 1.11]
test_data[:, 1, 1, 2] = [0.2, 0.1, 0.1, 1.0]
test_data[:, 1, 2, 2] = [0.6, 0.3, 0.50, 1.0]
test_data[:, 2, 1, 2] = [0.3, 0.4, 0.4, 0.4]
test_data[:, 2, 2, 2] = [1.0, 0.8, 1.0, 1.0]

# Make a solution for the test data.
validating_set = zeros(1, domain...)
indicators_dim = vcat(0, indicators,18000)
validating_set[1, 1, 1, 1] = indicators_dim[3] +
                             ((0.5 - 0.3) / (0.55 - 0.3)) *
                             (indicators_dim[4] - indicators_dim[3])
validating_set[1, 1, 2, 1] = indicators_dim[3] +
                             ((0.5 - 0.35) / (0.5 - 0.35)) *
                             (indicators_dim[4] - indicators_dim[3])
validating_set[1, 2, 1, 1] = indicators_dim[1] +
                             ((0.5 - 0.0) / (0.5 - 0.0)) *
                             (indicators_dim[2] - indicators_dim[1])
validating_set[1, 2, 2, 1] = indicators_dim[1] +
                             ((0.5 - 0.0) / (0.6 - 0.0)) *
                             (indicators_dim[2] - indicators_dim[1])
validating_set[1, 1, 1, 2] = indicators_dim[4] +
                             ((0.5 - 0.15) / (1 - 0.15)) *
                             (indicators_dim[5] - indicators_dim[4])
validating_set[1, 1, 2, 2] = indicators_dim[3] +
                             ((0.5 - 0.45) / (0.55 - 0.45)) *
                             (indicators_dim[4] - indicators_dim[3])
validating_set[1, 2, 1, 2] = indicators_dim[5] +
                             ((0.5 - 0.4) / (1 - 0.4)) *
                             (indicators_dim[6] - indicators_dim[5])
validating_set[1, 2, 2, 2] = indicators_dim[1] +
                             ((0.5 - 0.0) / (0.9 - 0.0)) *
                             (indicators_dim[2] - indicators_dim[1])

# Run the order relations functions
percentile = [50]
output_grid = ordrel(test_data, indicators, domain, percentile)

# validate if the two grids are equal.
@test output_grid ≈ validating_set
