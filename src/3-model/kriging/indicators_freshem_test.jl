using DataFrames
using Test

include("functions.jl")

# Set indicator thresholds
indicators = [150, 500, 1000, 3000, 5000, 10000, 15000]

# Test Dataset
p10 = [6748.0, 0.0]
p25 = [10062.0, 691.0]
p40 = [13500.0, 1425.0]
p50 = [16309.0, 2002.0]
p60 = [18000.0, 2884.0]
p75 = [18000.0, 4264.0]
p90 = [18000.0, 10021.0]

df_test = DataFrame(
  Cl_p10 = p10,
  Cl_p25 = p25,
  Cl_p40 = p40,
  Cl_p50 = p50,
  Cl_p60 = p60,
  Cl_p75 = p75,
  Cl_p90 = p90,
)

# Validation Dataset
Ind_150_val_1 = 0.0
Ind_150_val_2 = (10 + ( (150.0-0.0)/(691.0-0.0) * (25-10) ) ) / 100
Ind_500_val_1 = 0.0
Ind_500_val_2 = (10 + ( (500.0-0.0)/(691.0-0.0) * (25-10) ) ) / 100
Ind_1000_val_1 = 0.0
Ind_1000_val_2 = (25 + ( (1000.0-691.0)/(1425.0-691.0) * (40-25) ) ) / 100
Ind_3000_val_1 = 0.0
Ind_3000_val_2 = (60 + ( (3000.0-2884.0)/(4264.0-2884.0) * (75-60) ) ) / 100
Ind_5000_val_1 = 0.0
Ind_5000_val_2 = (75 + ( (5000.0-4264.0)/(10021.0-4264.0) * (90-75) ) ) / 100
Ind_10000_val_1 = (10 + ( (10000.0-6748.0)/(10062.0-6748.0) * (25-10) ) ) / 100
Ind_10000_val_2 = (75 + ( (10000.0-4264.0)/(10021.0-4264.0) * (90-75) ) ) / 100
Ind_15000_val_1 = (40 + ( (15000.0-13500.0)/(16309.0-13500.0) * (50-40) ) ) / 100
Ind_15000_val_2 = 1.0

df_val = DataFrame(
    I_150 = [Ind_150_val_1,Ind_150_val_2],
    I_500 = [Ind_500_val_1,Ind_500_val_2],
    I_1000 = [Ind_1000_val_1,Ind_1000_val_2],
    I_3000 = [Ind_3000_val_1,Ind_3000_val_2],
    I_5000 = [Ind_5000_val_1,Ind_5000_val_2],
    I_10000 = [Ind_10000_val_1,Ind_10000_val_2],
    I_15000 = [Ind_15000_val_1,Ind_15000_val_2],
)

# Run the indicators_freshem function on the test data
df_calc = indicators_freshem(df_test,indicators)

# validate if the two grids are equal.
@test Matrix(df_calc) ≈ Matrix(df_val)
