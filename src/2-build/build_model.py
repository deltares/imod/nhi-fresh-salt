# -*- coding: utf-8 -*-
"""
This script creates the iMOD-WQ model, by following
the following steps:
1. Open created packages from saved netcdf files
2. Build the model with the imod model builder using the opened input
3. Write the model (with imod default values, constants and input parameters)
4. Visualize the model input in 2d maps


Input:
- data/2-interim/conductivity.nc
- data/2-interim/template.nc
- data/2-interim/top_bot.nc
- data/2-interim/bnd.nc
- data/2-interim/drain.nc
- data/2-interim/river.nc
- data/2-interim/ghb.nc
- data/2-interim/starting_concentration.nc
- data/2-interim/recharge.nc
- data/2-interim/wel/negative_well*.ipf

Output:
- iMOD-WQ model input + runfile
"""
import numpy as np
import os
import imod
from pandas import date_range
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

try:
    modelname = snakemake.params.modelname
except NameError:
    modelname = 'LHM4.1.2'



print(f"Building model {modelname}")
path_bas = snakemake.input.path_bas    
path_lpf = snakemake.input.path_lpf    
path_btn = snakemake.input.path_btn    
path_ani = snakemake.input.path_ani
path_hfb = snakemake.input.path_hfb
path_rch = snakemake.input.path_rch    
path_drn_b = snakemake.input.path_drn_b     
path_drn_mvgrep = snakemake.input.path_drn_mvgrep    
path_drn_sof = snakemake.input.path_drn_sof     
path_boils = snakemake.input.path_boils
path_riv_h = snakemake.input.path_riv_h    
path_riv_p = snakemake.input.path_riv_p    
path_riv_s = snakemake.input.path_riv_s    
path_riv_t = snakemake.input.path_riv_t    
path_ghb = snakemake.input.path_ghb     
path_wel = snakemake.input.path_wel     
path_riv_h_drn = snakemake.input.path_riv_h_drn
path_riv_p_drn = snakemake.input.path_riv_p_drn
path_riv_s_drn = snakemake.input.path_riv_s_drn
rivdrn_as_river = snakemake.params.rivdrn_as_river
resultdir_is_workdir = snakemake.params.resultdir_is_workdir
path_data = Path(snakemake.params.path_data)
"""
path_bas = "data/2-interim/bas.nc"
path_lpf = "data/2-interim/adaptations/lpf.nc"
path_btn = "data/2-interim/btn.nc"
path_rch = "data/2-interim/adaptations/recharge.nc"
path_drn_b = "data/2-interim/drainage_b.nc"
path_drn_mvgrep = "data/2-interim/adaptations/drainage_mvgrep.nc"
path_drn_sof = "data/2-interim/adaptations/drainage_sof.nc"
path_boils = "data/2-interim/boils.nc"
path_riv_h = "data/2-interim/river_h.nc"
path_riv_p = "data/2-interim/adaptations/river_p.nc"
path_riv_s = "data/2-interim/river_s.nc"
path_riv_t = "data/2-interim/river_t.nc"
path_riv_h_drn = "data/2-interim/river_h_drn.nc"
path_riv_p_drn = "data/2-interim/adaptations/river_p_drn.nc"
path_riv_s_drn = "data/2-interim/adaptations/river_s_drn.nc"
path_ghb = "data/2-interim/ghb.nc"
path_wel = "data/2-interim/wel.nc"
"""
# Build the model from cache
m = imod.wq.SeawatModel(modelname)
m["bas"] = imod.wq.BasicFlow.from_file(path_bas)
m["lpf"] = imod.wq.LayerPropertyFlow.from_file(path_lpf)
m["btn"] = imod.wq.BasicTransport.from_file(path_btn)
m["adv"] = imod.wq.AdvectionTVD(courant=1.0)
m["dsp"] = imod.wq.Dispersion(longitudinal=1.0, diffusion_coefficient=0.0000864)
m["vdf"] = imod.wq.VariableDensityFlow(density_concentration_slope=1.316)
m["ani"] = imod.wq.HorizontalAnisotropy.from_file(path_ani)
m["hfb"] = imod.wq.HorizontalFlowBarrier(path_hfb)

# Boundary conditions
m["rch"] = imod.wq.RechargeHighestActive.from_file(path_rch)

m["drn_b"] = imod.wq.Drainage.from_file(path_drn_b)
m["drn_sof"] = imod.wq.Drainage.from_file(path_drn_sof)
m["drn_drainage_mvgrep"] = imod.wq.Drainage.from_file(path_drn_mvgrep)
m["drn_boils"] = imod.wq.Drainage.from_file(path_boils)

m["riv_h"] = imod.wq.River.from_file(path_riv_h)
m["riv_p"] = imod.wq.River.from_file(path_riv_p)
m["riv_s"] = imod.wq.River.from_file(path_riv_s)
m["riv_t"] = imod.wq.River.from_file(path_riv_t)
if rivdrn_as_river:
    m["riv_h_drn"] = imod.wq.River.from_file(path_riv_h_drn)
    m["riv_p_drn"] = imod.wq.River.from_file(path_riv_p_drn)
    m["riv_s_drn"] = imod.wq.River.from_file(path_riv_s_drn)
else:
    m["riv_h_drn"] = imod.wq.Drainage.from_file(path_riv_h_drn)
    m["riv_p_drn"] = imod.wq.Drainage.from_file(path_riv_p_drn)
    m["riv_s_drn"] = imod.wq.Drainage.from_file(path_riv_s_drn)

m["ghb"] = imod.wq.GeneralHeadBoundary.from_file(path_ghb)
m["wel"] = imod.wq.Well.from_file(path_wel)

# Solvers and output
m["pksf"] = imod.wq.ParallelKrylovFlowSolver(
    max_iter=150,
    inner_iter=100,
    hclose=0.0001,
    rclose=2000.0,
    relax=0.98,
    partition="rcb",
)
m["pkst"] = imod.wq.ParallelKrylovTransportSolver(
    max_iter=150, inner_iter=50, cclose=1.0e-6, partition="rcb"
)
m["oc"] = imod.wq.OutputControl(
    save_head_idf=True, save_concentration_idf=True, save_budget_idf=True
)

start = np.datetime64("2000-01-01T00:00")
endss = np.datetime64("2000-01-01T00:01")
end100y = np.datetime64("2100-01-01T00:00")
daterange = date_range(start, end100y, freq="1AS")

if snakemake.params.steadystate:  # steady-state
    m["wel"].save_budget = True
    m["lpf"].save_budget = True
    m["drn_b"].save_budget = True
    m["drn_sof"].save_budget = True
    m["drn_drainage_mvgrep"].save_budget = True
    m["drn_boils"].save_budget = True
    m["riv_h"].save_budget = True
    m["riv_p"].save_budget = True
    m["riv_s"].save_budget = True
    m["riv_t"].save_budget = True
    m["ghb"].save_budget = True 
    m.time_discretization([start, endss])
else: # transient 100y
    m["lpf"].save_budget = True
    m["pksf"] = imod.wq.ParallelKrylovFlowSolver(
        max_iter=1000,
        inner_iter=500,
        hclose=0.001,
        rclose=10000.0,
        relax=0.98,
        partition="rcb",
    )
    m.time_discretization(daterange)

m["time_discretization"]["transient"] = False
print("Writing model")
m.write(path_data / f"3-input/{modelname}", result_dir=path_data / f"4-output/{modelname}", resultdir_is_workdir=resultdir_is_workdir)

if snakemake.params.visualize:
    print("visualizing")
    m.visualize(f"reports/figures_{modelname}")
