import imod
import numpy as np
import pandas as pd
import geopandas as gpd
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
import subprocess
from pathlib import Path

version = "v2.3.0"

# filenames
fn_transects = "data/1-external/cross_section_locations/combined.shp"
fn_cl = f"data/4-output/{version}/conc/conc*"
fn_topbot = "data/2-interim/layermodel.nc"
fn_conductivity = "data/2-interim/conductivity.nc"
fn_legend = "data/1-external/legends/chloride_gL.leg"
outputdir = Path(f"./reports/videos/{version}")
add_streamfunction = True  # flag as not in imod-python master
add_quiver = True
fn_frf = f"data/4-output/{version}/bdgfrf/bdgfrf*"
fn_fff = f"data/4-output/{version}/bdgfff/bdgfff*"
fn_flf = f"data/4-output/{version}/bdgflf/bdgflf*"
overwrite = False

############ 3D Chloride #############
cl = imod.idf.open(fn_cl)
layermodel = xr.open_dataset(fn_topbot)
conductivity = xr.open_dataset(fn_conductivity)
is_aquitard = (conductivity["kh"] < 1.0).astype(int)

if add_streamfunction or add_quiver:
    frf = imod.idf.open(fn_frf)
    fff = imod.idf.open(fn_fff)
    flf = imod.idf.open(fn_flf)

########### Create cross sections ############
colors, levels = imod.visualize.read_imod_legend(fn_legend)

# read transects from shapefile
df_transects = gpd.read_file(fn_transects)

outputdir.mkdir(exist_ok=True, parents=True)

for name, transect in zip(df_transects.Name, df_transects.geometry):
    if "Zandvoort" in name:
        break
    if (
        overwrite
        or not (outputdir / f"crosssection_{name.replace(' ','')}.mp4").exists()
    ):
        print(name)
        break

        nrows = 1  # len(cl.time)
        # fig, axes = plt.subplots(nrows=nrows, squeeze=False, figsize=(6.4, 3.2 * nrows))
        # fig.suptitle(f"Transect {name}")

        cmap = None
        norm = None
        top = imod.select.cross_section_linestring(layermodel["top"], transect).compute()
        bot = imod.select.cross_section_linestring(layermodel["bot"], transect).compute()
        z = 0.5 * top + 0.5 * bot
        aquitards = imod.select.cross_section_linestring(is_aquitard, transect).compute()
        aquitards = aquitards.assign_coords(top=top)
        aquitards = aquitards.assign_coords(bottom=bot)
        aquitards.coords["ds"] = np.abs(aquitards.coords["ds"])

        ### select cross section
        da = imod.select.cross_section_linestring(cl, transect).compute()
        da = da.assign_coords(top=top)
        da = da.assign_coords(bottom=bot)
        da.coords["ds"] = np.abs(da.coords["ds"])
        # print(da)

        # stream function
        if add_streamfunction:
            sf = imod.evaluate.streamfunction_linestring(
                frf, fff, transect
            ).compute()
            sf = sf.assign_coords(top=top)
            sf = sf.assign_coords(bottom=bot)
            sf.coords["ds"] = np.abs(sf.coords["ds"])
        #break 

        # quiver
        if add_quiver:
            qu, qv = imod.evaluate.quiver_linestring(
                frf, fff, flf, transect
            )#.compute()
            qu = qu.assign_coords(top=top)
            qu = qu.assign_coords(bottom=bot)
            qu.coords["ds"] = np.abs(qu.coords["ds"])
            qv = qv.assign_coords(top=top)
            qv = qv.assign_coords(bottom=bot)
            qv.coords["ds"] = np.abs(qv.coords["ds"])


        # run for each percentile
        for i, t in enumerate(cl.time.values):  # ["p50"]:#

            fig, ax = plt.subplots(figsize=(6.4, 3.2))
            fig.suptitle(f"Transect {name}")

            fig, ax = imod.visualize.cross_section(
                da.isel(time=i),
                colors=colors,
                levels=levels,
                layers=False,
                aquitards=aquitards,
                return_cmap_norm=False,
                fig=fig,
                ax=ax,
            )
            #break
            if add_streamfunction:
                imod.visualize.streamfunction(sf.isel(time=i), ax, 20)
            if add_quiver:
                thin = {"s":slice(0,None,5), "layer":slice(0,None,3)}  # thin out quivers, only plot at every other .. cell
                imod.visualize.quiver(qu.isel(time=i).isel(**thin), qv.isel(time=i).isel(**thin), ax)
            ax.set_ylim(bottom=-250.0, top=25.0)
            ax.set_title(f"Tijdstap {pd.Timestamp(t):%Y%m%d}")

            plt.tight_layout(rect=(0, 0, 1, 0.95))
            plt.savefig(
                outputdir / f"crosssection_{name.replace(' ','')}_{i:04d}.png",
                dpi=300,
                #    bbox_inches="tight",
            )  # save w/o measurements
            plt.close()

        fn = outputdir / f"crosssection_{name.replace(' ','')}.mp4"
        if fn.exists():
            fn.unlink()

        subprocess.run(
            [
                "ffmpeg",
                "-r",
                "3",
                "-f",
                "image2",
                "-s",
                "1920x960",
                "-i",
                f"{str(outputdir)}\\crosssection_{name.replace(' ','')}_%04d.png",
                "-vcodec",
                "libx264",
                "-crf",
                "25",
                "-pix_fmt",
                "yuv420p",
                f"{str(outputdir)}\\crosssection_{name.replace(' ','')}.mp4",
            ]
        )
