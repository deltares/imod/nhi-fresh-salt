import xarray as xr
import imod
import numpy as np

from pathlib import Path

# Some helper functions
def layer_coord_to_var(da_layered):
    """
    Convert xr.DataArray with layers as coordinates to a xr.Dataset
    with a seperate data variable for each layer.
    """
    da_ls = []
    for layer, da in da_layered.groupby("layer"):
        name = "{}_layer_{}".format(da.name, layer)
        da = da.reset_coords(names=["layer"], drop=True).rename(name)
        da_ls.append(da)
    return xr.merge(da_ls)


def top_3d(bottom, surface):
    """
    From a surface (2d array) and bottom (3d array),
    create a 3d top array.
    """
    # Assign layers dimensions to surface.
    surface = surface.assign_coords(layer=1)
    # Create 3D array of tops
    ## Roll bottom one layer downward: the bottom of a layer is top of next layer
    top = bottom.roll(layer=1, roll_coords=False)
    ## Remove layer 1
    top = top.sel(layer=slice(2, None))
    ## Add surface as layer 1
    top = xr.concat([surface, top], dim="layer")
    return top


# Path management
path_cl = Path("data/2-interim/3dchloride_groundwater_gL.nc")
result_dir = Path("data/5-visualization/3dchloride_groundwater_gL")

ugrid_nc = result_dir / f"{path_cl.stem}_ugrid.nc"

# Open data
cl = xr.open_dataarray(path_cl).to_dataset(dim="percentile")
cl = cl.rename(p25="chloride_p25",p50="chloride_p50",p75="chloride_p75")
bottom = cl.bottom#.values
top = cl.top#.values
cl = cl.drop(["z","dz","top","bottom"])
bottom = bottom.drop(["z","dz","top","bottom"])
top = top.drop(["z","dz","top","bottom"])
#
bottom3d = xr.ones_like(cl["chloride_p50"]) * bottom
bottom3d.name = "bottom"
top3d = xr.ones_like(cl["chloride_p50"]) * top
top3d.name = "top"
# Convert
# Convert layered DataArrays to Dataset
# with seperate variables for each layer
ds = xr.merge(layer_coord_to_var(da) for da in [bottom3d, top3d, cl["chloride_p25"], cl["chloride_p50"], cl["chloride_p75"]])

# Convert to URGRID
ds_ugrid = imod.util.to_ugrid2d(ds)

# Due to a bug in MDAL, we have to encode the times as floats
# instead of integers
# when this is fixed: https://github.com/lutraconsulting/MDAL/issues/348
#ds_ugrid["time"].encoding["dtype"] = np.float64


# Write
ds_ugrid.to_netcdf(ugrid_nc)
