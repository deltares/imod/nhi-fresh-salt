import geopandas as gpd
import xarray as xr
import matplotlib.pyplot as plt
from pathlib import Path
import imod

# plot years:
# 2078, 2129, 2277 - 0.5, 1, 3m
years = [2078, 2129, 2277]
slrs = [0.5, 1., 3.]
path_interim = Path("d:/nhi-fresh-salt/data/2-interim")
path_output = Path("reports/figures/kpslr")
prov = gpd.read_file("d:/nhi-fresh-salt/data/1-external/shapes/provincie.shp")
colors,levels = imod.visualize.read_imod_legend("d:/nhi-fresh-salt/data/1-external/legends/residuals.leg")
overlay = {"gdf":prov, "fc":"None", "ec":"k"}
path_output.mkdir(parents=True, exist_ok=True)

# GHB en RIVH, RIVP, S en T in aparte kaart
ghb = xr.open_dataset(path_interim / "ghb_kpslr.zarr", engine="zarr")
rivh = xr.open_dataset(path_interim / "river_h_kpslr.zarr", engine="zarr")
rivp = xr.open_dataset(path_interim / "river_p_kpslr.zarr", engine="zarr")
rivs = xr.open_dataset(path_interim / "river_s_kpslr.zarr", engine="zarr")
rivt = xr.open_dataset(path_interim / "river_t_kpslr.zarr", engine="zarr")
drnsof = xr.open_dataset(path_interim / "drainage_sof_kpslr.zarr", engine="zarr")

ghb_t0 = ghb.isel(time=0, drop=True)["head"]
rivh_t0 = rivh.isel(time=0, drop=True)["stage"]
rivp_t0 = rivp.isel(time=0, drop=True)["stage"]
rivs_t0 = rivs.isel(time=0, drop=True)["stage"]
rivt_t0 = rivt.isel(time=0, drop=True)["stage"]
drnsof_t0 = drnsof.isel(time=0, drop=True)["elevation"].max(dim="layer")
# combine
da_t0 = ghb_t0.max(dim="layer").combine_first(rivh_t0.max(dim="layer"))
da2_t0 = rivp_t0.max(dim="layer").combine_first(rivs_t0.max(dim="layer")).combine_first(rivt_t0.max(dim="layer"))
for y, slr in zip(years, slrs):
    ghb_y = ghb.sel(time=f"{y}-01-01", method="pad")["head"].squeeze(drop=True)
    rivh_y = rivh.sel(time=f"{y}-01-01", method="pad")["stage"].squeeze(drop=True)
    # combine
    da_y = ghb_y.combine_first(rivh_y.max(dim="layer"))
    diff = da_y - da_t0
    fig, ax = imod.visualize.plot_map(diff, colors, levels, [overlay], kwargs_colorbar={"label":f"Difference waterlevel at SLR {slr} m ({y})"})
    _ = ax.set_xticklabels([])
    _ = ax.set_yticklabels([])
    plt.savefig(path_output / f"ghbrivh_{y}.png", bbox_inches="tight")
    plt.close()

    if slr != slrs[-1]:
        rivp_y = rivp.sel(time=f"{y}-01-01", method="pad")["stage"].squeeze(drop=True)
        rivs_y = rivs.sel(time=f"{y}-01-01", method="pad")["stage"].squeeze(drop=True)
        rivt_y = rivt.sel(time=f"{y}-01-01", method="pad")["stage"].squeeze(drop=True)
        drnsof_y = drnsof.sel(time=f"{y}-01-01", method="pad")["elevation"].squeeze(drop=True).max(dim="layer")
    else:
        rivp_y = rivp.isel(time=-1)["stage"]
        rivs_y = rivs.isel(time=-1)["stage"]
        rivt_y = rivt.isel(time=-1)["stage"]
        drnsof_y = drnsof.isel(time=-1)["elevation"].max(dim="layer")

    # combine
    da_y = rivp_y.max(dim="layer").combine_first(rivs_y.max(dim="layer")).combine_first(rivt_y.max(dim="layer"))
    diff = da_y - da2_t0
    fig, ax = imod.visualize.plot_map(diff, colors, levels, [overlay], kwargs_colorbar={"label":f"Difference waterlevel at SLR {slr} m ({y})"})
    _ = ax.set_xticklabels([])
    _ = ax.set_yticklabels([])
    plt.savefig(path_output / f"rivpst_{y}.png", bbox_inches="tight")
    plt.close()

    # drn
    diff = drnsof_y - drnsof_t0
    fig, ax = imod.visualize.plot_map(diff, colors, levels, [overlay], kwargs_colorbar={"label":f"Difference elevation at SLR {slr} m ({y})"})
    _ = ax.set_xticklabels([])
    _ = ax.set_yticklabels([])
    plt.savefig(path_output / f"drnsof_{y}.png", bbox_inches="tight")
    plt.close()

