
## Remove combinations of boundary conditions that result in high intra-cell fluxes

import scipy.ndimage
import imod
import numpy as np
import os
import xarray as xr
import sys
import pandas as pd
import gc
from pathlib import Path

sys.path.append(os.path.dirname(__file__))  # add script directory to python path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

#paths
path_topbot_orig = snakemake.input.path_topbot_orig
path_ghb_cond = snakemake.input.path_ghb_cond
path_ghb_head = snakemake.input.path_ghb_head
path_ghb_dens = snakemake.input.path_ghb_dens
path_riv_H_cond = snakemake.input.path_riv_H_cond
path_riv_H_head = snakemake.input.path_riv_H_head
path_riv_H_bot = snakemake.input.path_riv_H_bot
path_riv_H_dens = snakemake.input.path_riv_H_dens 
path_riv_P_cond = snakemake.input.path_riv_P_cond
path_riv_P_head = snakemake.input.path_riv_P_head 
path_riv_P_bot = snakemake.input.path_riv_P_bot 
path_riv_S_cond = snakemake.input.path_riv_S_cond 
path_riv_S_head = snakemake.input.path_riv_S_head 
path_riv_S_bot = snakemake.input.path_riv_S_bot 
path_riv_T_cond = snakemake.input.path_riv_T_cond 
path_riv_T_head = snakemake.input.path_riv_T_head 
path_riv_T_bot = snakemake.input.path_riv_T_bot 
path_drn_B_lvl = snakemake.input.path_drn_B_lvl 
path_drn_B_cond = snakemake.input.path_drn_B_cond 
path_drn_mvgrep_lvl = snakemake.input.path_drn_mvgrep_lvl
path_drn_mvgrep_cond = snakemake.input.path_drn_mvgrep_cond
path_drn_SOF_lvl = snakemake.input.path_drn_SOF_lvl
path_drn_SOF_cond  = snakemake.input.path_drn_SOF_cond
path_shd = snakemake.input.path_shd
path_well_nhi = snakemake.input.path_well_nhi
path_external = Path(snakemake.params.path_external)
path_interim = Path(snakemake.params.path_interim)

top_bot = xr.open_dataset(path_topbot_orig)#fpath_interim / "top_bot_orig.nc"

#shd in cells Row 1196, Col 893 have ibound -1 but shd 0.0, fill with value 77.2 
shd = imod.idf.open(path_shd).squeeze('time',drop=True).load()
shd[:,1195,892] = 77.2
#imod.idf.save(r"data/1-external/shd/shd", shd)
shd.to_netcdf(path_interim / "shd_corr.nc")
ghb = xr.Dataset()
ghb['conductance'] = imod.idf.open(path_ghb_cond) # r"data\1-external\ghb\ghb_cond_l1.idf"
ghb['head'] = imod.idf.open(path_ghb_head) # r"data\1-external\ghb\ghb_head_l1.idf"
ghb['density'] = imod.idf.open(path_ghb_dens) # r"data\1-external\ghb\ghb_dens_L1.IDF"
ghb.to_netcdf(path_interim / "ghb_ss_orig.nc")

#remove well akzonobel Deventer (not in use)
well_nhi = imod.ipf.read(path_well_nhi)# "data/1-external/wel/wel_nhi_all.ipf"
well_nhi = well_nhi.replace(-23427.6, 0 )
imod.ipf.write(path_external / r"wel/wel_nhi_all.ipf",well_nhi )

riv_h = xr.Dataset()
riv_h['conductance'] = imod.idf.open(path_riv_H_cond)#r"data\1-external\riv\hoofdwater\COND_H_L*"
riv_h['stage'] = imod.idf.open(path_riv_H_head)#r"data\1-external\riv\hoofdwater\steady-state\MEAN_PEILH_2011_2018.IDF"
riv_h['bottom_elevation'] = imod.idf.open(path_riv_H_bot)#r"data\1-external\riv\hoofdwater\steady-state\BOTH_J_L*"
riv_h['density'] = imod.idf.open(path_riv_H_dens)#r"data\1-external\riv\hoofdwater\riv_h_dens_L*"
riv_h.to_netcdf(path_interim / "riv_h_ss_orig.nc")

riv_p = xr.Dataset()
riv_p['conductance'] = imod.idf.open(path_riv_P_cond).squeeze("layer")
riv_p['stage'] = imod.idf.open(path_riv_P_head)#r"data\1-external\riv\regionaal\steady-state\PEIL_P1J_250.IDF"
riv_p['bottom_elevation'] = imod.idf.open(path_riv_P_bot)#r"data\1-external\riv\regionaal\steady-state\BODH_P1J_250.IDF"
riv_p.to_netcdf(path_interim / "riv_p_ss_orig.nc")

#remove primair river where conductance in riv_h and riv_p is high and the stage does not match
riv_h_cond_merge = riv_h.conductance.sel(layer=1).combine_first(riv_h.conductance.sel(layer=2))
is_riv_h = riv_h_cond_merge.notnull() & riv_h['stage'].notnull()
remove_p = (riv_p['conductance'] > 10_000) & is_riv_h & (riv_h['stage'] != riv_p['stage'])
riv_p = riv_p.where(~remove_p)
riv_p = riv_p.where(riv_p['conductance'] > 0.)
riv_p.to_netcdf(path_interim / "riv_p_ss_orig_corr.nc")

riv_s = xr.Dataset()
riv_s['conductance'] = imod.idf.open(path_riv_S_cond) #r"data\1-external\riv\regionaal\cond_s_l0.IDF"
riv_s['stage'] = imod.idf.open(path_riv_S_head) #r"data\1-external\riv\regionaal\steady-state\PEIL_S1J_250.IDF"
riv_s['bottom_elevation'] = imod.idf.open(path_riv_S_bot) #r"data\1-external\riv\regionaal\steady-state\BODH_S1J_250.IDF"
riv_s.to_netcdf(path_interim / "riv_s_ss_orig.nc")

riv_t = xr.Dataset()
riv_t['conductance'] = imod.idf.open(path_riv_T_cond) #r"data\1-external\riv\regionaal\cond_t_l0.IDF"
riv_t['stage'] = imod.idf.open(path_riv_T_head)# r"data\1-external\riv\regionaal\steady-state\PEIL_T1J_250.IDF"
riv_t['bottom_elevation'] = imod.idf.open(path_riv_T_bot) #r"data\1-external\riv\regionaal\steady-state\BODH_T1J_250.IDF"
riv_t.to_netcdf(path_interim / "riv_t_ss_orig.nc")


drn_b = xr.Dataset()
drn_b['elevation'] = imod.idf.open(path_drn_B_lvl) #r"data\1-external\drn\BODH_B_250.IDF"
drn_b['conductance'] = imod.idf.open(path_drn_B_cond) #r"data\1-external\drn\COND_B_250.IDF"
drn_b.to_netcdf(path_interim / "drn_b_orig.nc")

drn_mvgrep = xr.Dataset()
drn_mvgrep ['elevation'] = imod.idf.open(path_drn_mvgrep_lvl) #r"data\1-external\drn\BODH_BRP2012_MVGREP_250.IDF"
drn_mvgrep['conductance'] = imod.idf.open(path_drn_mvgrep_cond) #r"data\1-external\drn\COND_BRP2012_MVGREP_250.IDF"
drn_mvgrep.to_netcdf(path_interim / "drn_mvgrep_orig.nc")

drn_sof = xr.Dataset()
drn_sof['elevation'] = imod.idf.open(path_drn_SOF_lvl).load() #r"data\1-external\drn\BODH_SOF_250.IDF"
drn_sof['conductance'] = imod.idf.open(path_drn_SOF_cond).load() #r"data\1-external\drn\COND_SOF_250.IDF"
# cond drn_sof conflicting with other boundary conditions: Row 1136, Col 840 / Row 1195, Col 893 / Row 1195, 994
drn_sof.conductance[1135, 839] = np.nan
drn_sof.conductance[1194, 892] = np.nan
drn_sof.conductance[1194, 893] = np.nan
drn_sof.elevation[1135, 839] = np.nan
drn_sof.elevation[1194, 892] = np.nan
drn_sof.elevation[1194, 893] = np.nan
drn_sof['conductance'] = drn_sof.conductance.where(drn_sof.elevation > -10.0, np.nan)
drn_sof['elevation'] = drn_sof.elevation.where(drn_sof.elevation > -10.0, np.nan)
drn_sof.to_netcdf(path_interim / "drn_sof_orig.nc")


paths_ghb = {
    "ghb": path_interim / "ghb_ss_orig.nc",
}

paths_riv = {
    "riv_h": path_interim / "riv_h_ss_orig.nc",
    "riv_p": path_interim / "riv_p_ss_orig_corr.nc",
    "riv_s": path_interim / "riv_s_ss_orig.nc",
    "riv_t": path_interim / "riv_t_ss_orig.nc",
}
paths_drn = {
    "drn_b": path_interim / "drn_b_orig.nc",
    "drn_mvgrep": path_interim / "drn_mvgrep_orig.nc",
    "drn_sof": path_interim / "drn_sof_orig.nc",
}

ghb = {k:xr.open_dataset(v).load() for k,v in paths_ghb.items()}
riv = {k:xr.open_dataset(v).load() for k,v in paths_riv.items()}
drn = {k:xr.open_dataset(v).load() for k,v in paths_drn.items()}
#ghb = {k:xr.open_dataset(v) for k,v in paths_ghb.items()}
#riv = {k:xr.open_dataset(v) for k,v in paths_riv.items()}
#drn = {k:xr.open_dataset(v) for k,v in paths_drn.items()}

# top_bot not good approximation for layer thickness of Holocene layer 1
top_bot["dz"] = top_bot["top"] - top_bot["bot"]
top_bot["dz"].loc[{"layer":1}] = np.fmax(2., top_bot["top"].sel(layer=1,drop=True) - top_bot["top"].sel(layer=2,drop=True))
top_bot["dz"].loc[{"layer":2}] = np.fmax(2., top_bot["top"].sel(layer=2,drop=True) - top_bot["top"].sel(layer=3,drop=True))
top_bot["dz"] = np.fmax(2, top_bot["dz"]) # assume minimum of 2m
dt_min, dt_all = imod.evaluate.intra_cell_boundary_conditions(top_bot, ghb=ghb, riv=riv, drn=drn)

small_dt = dt_all.where(dt_all < 50.)
for dim in small_dt.dims:
    small_dt = small_dt.dropna(dim=dim,how="all")
small_dt.name = "small_dt"
small_dt = small_dt.drop(["dx","dy"])
small_dt = small_dt.to_dataset()
#small_dt.to_netcdf("data/tmp/small_dt.nc")
#small_dt = xr.open_dataset("data/tmp/small_dt.nc",chunks={"combination":1,"layer":1})["small_dt"]
gc.collect()
small_dt_df = small_dt.to_dataframe().dropna()

small_dt_df = small_dt_df.reset_index()
indices = imod.select.points_indices(top_bot,x=small_dt_df["x"],y=small_dt_df["y"],layer=small_dt_df["layer"])
small_dt_df["z"] = 0.5*top_bot["top"].isel(**indices) + 0.5*top_bot["bot"].isel(**indices) 
small_dt_df["source"] = small_dt_df.combination.apply(lambda x:x.split("-")[0])
small_dt_df["sink"] = small_dt_df.combination.apply(lambda x:x.split("-")[1])

small_dt_df = small_dt_df.reindex(columns=["x","y","z","layer","combination","source","sink","small_dt"])
small_dt_df.to_csv(path_interim / "small_dt_boundarycond.csv")
imod.ipf.write(path_interim / "small_dt_boundarycond_kpslr.ipf", small_dt_df)

del small_dt, dt_min, dt_all

ghb = {k:xr.open_dataset(v) for k,v in paths_ghb.items()}
riv = {k:xr.open_dataset(v) for k,v in paths_riv.items()}
drn = {k:xr.open_dataset(v) for k,v in paths_drn.items()}
gc.collect()

# as points_set_values fils on missing layer
def _reindex(ds):
    coords = dict(x=ds.x,y=ds.y)
    if "layer" in ds.dims:
        coords["layer"] = np.arange(float(ds.coords["layer"].min()),float(ds.coords["layer"].max())+1.)
    ds = ds.assign_coords({"dlayer":1})
    return ds.reindex(coords)

def _dataset_set_points_values(ds, values, **coords):
    #print(ds)
    ds = _reindex(ds)
    #coords["layer"] = coords["layer"].astype(float) + 0.5
    coords = {k:v for k,v in coords.items() if k in ds.dims}
    b = imod.select.points_in_bounds(ds, **coords)
    coords = {k:v[b] for k,v in coords.items()}

    for dvar in ds.data_vars:
        ds[dvar] = ds[dvar].load()
        selcoords = {k:v for k,v in coords.items() if k in ds[dvar].dims}
        ds[dvar] = imod.select.points_set_values(ds[dvar], values, **selcoords)
        #try:
        #    ds[dvar] = values
        #except ValueError:
        #    print("Valueerror?")
        #    pass
    return ds

corrected = []
# throw out sink, not source
for c in small_dt_df.sink.unique():
    print(c)
    sel = small_dt_df.loc[small_dt_df.sink == c]

    if "riv" in c:
        riv[c] = _dataset_set_points_values(riv[c], np.nan, x=sel.x.values, y=sel.y.values, layer=sel.layer.values)
    elif "ghb" in c:
        ghb[c] = _dataset_set_points_values(ghb[c], np.nan, x=sel.x.values, y=sel.y.values, layer=sel.layer.values)
    elif "drn" in c:
        drn[c] = _dataset_set_points_values(drn[c], np.nan, x=sel.x.values, y=sel.y.values, layer=sel.layer.values)

    corrected.append(c)
    gc.collect()

def _append_corr(path):
    path = Path(path)
    if "_corr" not in str(path):
        path = path.parent / f"{path.stem}_corr{path.suffix}"
    else:
        path = path.parent / f"{path.stem}2{path.suffix}"
    return path

# save corrected
for c in corrected:

    if "riv" in c:
        fn = _append_corr(paths_riv[c])
        riv[c].to_netcdf(fn)
    elif "ghb" in c:
        fn = _append_corr(paths_ghb[c])
        ghb[c].to_netcdf(fn)
    elif "drn" in c:
        fn = _append_corr(paths_drn[c])
        drn[c].to_netcdf(fn)
    print(f"Saved corrected {c} to {str(fn)}")
