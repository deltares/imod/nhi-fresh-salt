import geopandas as gpd
import imod
import os
import numpy as np
import pandas as pd
import xarray as xr

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# Paths
path_layermodel = "data/2-interim/layermodel.nc"
path_sconc = "data/1-external/conc/CONC*"
path_bnd = "data/2-interim/bnd.nc"
path_template = "data/2-interim/template.nc"
path_sea = "data/2-interim/water_masks.nc"
bnd = imod.idf.open(r"data/2-interim/bnd/ibound_l*")
# Open LHMzz layermodel
layermodel = xr.open_dataset(path_layermodel)
new_top = layermodel["top"]
new_bot = layermodel["bot"]
sconc = imod.idf.open(path_sconc)
sea_2d = xr.open_dataset(path_sea)["sea_2d"]
sconc_dz = np.array([0]+[5.]*2+[2.]*10+[5.]*8+[20.]*11)   

# make sure it fits top and bot of model: extend upper and lower layer
sconc_top = 10
sconc_bot = -280
sconc_dz[1] += np.ceil(new_top.max().values) - sconc_top
sconc_top = np.ceil(new_top.max().values)
sconc_dz[-1] += sconc_bot - np.floor(new_bot.min().values)
sconc_bot = np.floor(new_bot.min().values)
sconc_topbot = sconc_top - sconc_dz.cumsum()

# make dataarrays
dl = [xr.full_like(sconc.isel(layer=1),t) for t in sconc_topbot]
sconc_top = xr.concat(dl[:-1],pd.Index(range(1,len(sconc_topbot)), name='layer'))
sconc_bot = xr.concat(dl[1:],pd.Index(range(1,len(sconc_topbot)), name='layer'))

# Layerregridder werkt nog niet
#sconc_new = imod.prepare.LayerRegridder(method="mean").regrid(sconc,source_top=sconc_top, source_bottom=sconc_bot, destination_top=new_top, destination_bottom=new_bot)

# crude mapping based on midpoint of layer
sconc_new = bnd.copy()
sconc_new = sconc_new.load()
midpoint = 0.5*new_top+0.5*new_bot
#~np.isnan(dz.where((new_top >= riv_bot_s1_l1) & (new_bot <= riv_bot_s1_l1)))
for l in bnd.layer.values:
    sconc_new.loc[l] = sconc.where((sconc_top>=midpoint.sel(layer=l))&(sconc_bot<midpoint.sel(layer=l))).max(dim="layer")
sconc_new /= 1000.
# fill with 0 values on land, and 16.8 out in sea
sconc_new = sconc_new.where(~(sea_2d==1 & sconc_new.isnull()), 16.8).fillna(0)
sconc_new.to_netcdf("data/2-interim/starting_concentration.nc")
