import os, glob
from zipfile import ZipFile
#from time import strptime #asctime,
from datetime import datetime, timedelta
from os.path import join, exists, basename#, abspath
from shutil import copy, move, rmtree
import imod
import numpy as np
import xarray as xr
import pandas as pd
#import raster_func as rf
#from multiprocessing import Pool
#import warnings
#warnings.filterwarnings("ignore",category =RuntimeWarning)

def get_runzip(y,scen='S2085BP18'):
    basedir = r'p:\11202240-kpp-dp-zoetwater\NWM_BP2018_en_historie\2018_BP2018_productieomgeving\Modelzips_Productieomgeving_NWM_Z1\{}'.format(scen)
    
    loc = join(basedir,'ZW_LHM_{}*_{}.zip'.format(y+1,scen))
    
    fn = glob.glob(loc)
    if len(fn):
        print (fn[0])
    else:
        raise OSError('no zipfiles at location {}'.format(loc))
        
    return ZipFile(fn[0])

def get_timesteps(yearstart,yearend=None,start_or_end='start',reset_idec_year=False,dec_or_days='decade'):
    '''Iterator to get decadenr and date as ints for given year(s)'''
    if yearend is None: yearend = yearstart
    if dec_or_days == 'decade':
        if start_or_end == 'end': days = [10,20,31]
        else: days = [1,11,21]
            
        i=-1
        for year in range(yearstart,yearend+1):
            if reset_idec_year: i=-1
            for m in range(1,13):
                for d in days:
                    if d == 31:
                        # get actual last day
                        m1 = m
                        if m == 12: m1 = 0
                        d = (datetime(year,m1+1,1)-timedelta(days=1)).day
                    i += 1
                    yield i, year*10000+m*100+d
    else:
        # daily timesteps: use timedelta...
        for year in range(yearstart,yearend+1):
            d = datetime(year,1,1)
            i = 0
            while d.year == year:
                yield i, int(d.strftime("%Y%m%d"))
                d = d+timedelta(days=1)
                i += 1

def zipfn(fn):
    return os.path.normpath(fn).replace('\\','/')

def idfload(path,**kwargs):
    print ('Loading {}'.format(path))
    ret = imod.idf.open(path,**kwargs)
    return ret

def idfwrite(path,da,**kwargs):
    try:
        imod.idf.write(path,da.squeeze(drop=True),**kwargs)
    except ValueError:
        print(da.squeeze(drop=True))
        raise
    return None


bpscen = 'S2085BP18'
yearstart = 1973
yearend = 2003
#periods = [[1996,2006]]  # to do: create periods from startyear - endyear
periods = [[s,min(s+3,yearend)] for s in range(yearstart,yearend+1,4)]
#agricom_dir = r'p:\11202619-regioscan2\Agricom_WWL'

resultbasedir = r"c:\Users\romerove\OneDrive - Stichting Deltares\Documents\11203718-007-nhi-zoetzout\rch_2085"
resultdir = join(resultbasedir,bpscen)

filepatterns = {'msw_Ebs':join('msw_Ebs','msw_Ebs_<DATE>_l1.idf'),
                'msw_qinf':join('msw_qinf','msw_qinf_<DATE>_l1.idf'),
                'msw_Tact':join('msw_Tact','msw_Tact_<DATE>_l1.idf'),
}
prms = ['msw_qinf', 'msw_Ebs', 'msw_Tact']

agricomdict = {#'NamZnl':join(resultbasedir,'nl.idf'),
               'TimStart':'{}0101'.format(periods[0][0]),
               'TimEnd':'{}1231'.format(periods[0][1])}

if not exists(resultdir):
    os.mkdir(resultdir)

dir_to = resultdir
#dir_to = r"c:\Users\romerove\OneDrive - Stichting Deltares\Documents\11203718-007-nhi-zoetzout\rch_2085\REF2085BP18"

for period in periods:
    agricomdict['TimStart']='{}0101'.format(period[0])
    agricomdict['TimEnd']='{}1231'.format(period[1])
        # COPY METASWAP FILES
    for y in range(period[0],period[1]+1):
        with get_runzip(y,bpscen) as runzip:
            for prm in prms: # loop through parameters
                print(prm)
                fnto = join(dir_to,prm)
                decm1 = None
                for idec, dec in get_timesteps(y,start_or_end='end'):  # loop through all dates within period
                    fnfrom = join('Metaswap',filepatterns[prm].replace('<DATE>',str(dec)))
                    try:
                        zi = runzip.getinfo(zipfn(fnfrom))
                        zi.filename = basename(zi.filename)
                        runzip.extract(zi,fnto)
                        #if prm == 'transol_con_rz':
                        #    move(join(dir_to,prm,basename(fnfrom)),join(dir_to,filepatterns[prm].replace('<DATE>',str(dec))))
                    except KeyError:
                        # hack for transol files in leap years
                        copy(join(dir_to,filepatterns[prm].replace('<DATE>',str(decm1))),join(dir_to,filepatterns[prm].replace('<DATE>',str(dec))))
                    decm1 = dec

