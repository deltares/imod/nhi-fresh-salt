"""
This script creates the ghb package, by following
the following steps:
1. read GHB location from layermodel
2. calculate density from chloride
3. assign conc, dens, cond, stage values
4. create 3d ghb

Input:
- data/2-interim/template_2d.nc
- data/1-external/lagenmodel/layermodel.nc
- data/2-interim/bnd.nc

Output:
- data/2-interim/ghb.nc
"""
import os
import imod
import numpy as np
import xarray as xr
import numba
import sys

sys.path.append(os.path.dirname(__file__))  # add script directory to python path
from utils import assign_layer_3d, inner_join_dataarrays

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

@numba.njit
def _fill_by_layer(layer_id, values, out):
    nlay, nrow, ncol = layer_id.shape
    for k in range(nlay):
        for i in range(nrow):
            for j in range(ncol):
                n = layer_id[k, i, j]
                if np.isnan(n):
                    continue
                elif n%2:
                    v = values[int(n // 2), i, j]
                    out[k, i, j] = v
                else:
                    out[k, i, j] = np.nan

def fill_by_layer(layer_id, values):
    out = xr.full_like(layer_id, np.nan)
    _fill_by_layer(layer_id.values, values.values, out.values)
    return out


def create_ghb(
    stage_2d,
    cond_2d,
    conc_2d,
    dens_2d,
    like_2d=None,
    ibound=None,
    top_bot=None,
    method="z_then_upper",
):
    """
    Function to create consistent ghb xr.Dataset as input for
    imod.wq.GeneralHeadBoundary()
    """
    # conform to like_2d
    if like_2d is not None:
        stage_2d = imod.prepare.reproject(stage_2d, like_2d, method="nearest")
        cond_2d = imod.prepare.reproject(cond_2d, like_2d, method="nearest")
        conc_2d = imod.prepare.reproject(conc_2d, like_2d, method="nearest")
        dens_2d = imod.prepare.reproject(dens_2d, like_2d, method="nearest")

    # inner join bot and cond and make sure both exist
    ghb_2d = inner_join_dataarrays(
        head=stage_2d, cond=cond_2d, conc=conc_2d, dens=dens_2d
    )

    # assign to 3d layers using method
    assigned = assign_layer_3d(ghb_2d, ibound, top_bot, method)
    ghb_3d = ghb_2d.where(assigned["head"])

    # reselect only if in ibound
    ghb_3d = ghb_3d.where(ibound == 1)

    # assert all have equal no of nodata cells
    dvars = list(ghb_3d.data_vars.keys())
    for k1, k2 in zip(dvars[:-1], dvars[1:]):
        assert ghb_3d[k1].isnull().equals(ghb_3d[k2].isnull())

    return ghb_3d  # .dropna(dim="layer", how="all")


# Paths
path_2d_template = snakemake.input.path_template_2d #"data/2-interim/template_2d.nc"
path_template = snakemake.input.path_template #"data/2-interim/template.nc"
path_layermodel = snakemake.input.path_layermodel #"data/2-interim/layermodel.nc"
path_conc = snakemake.input.path_conc_corr #"data/2-interim/concentration_corrected.nc"
path_conc_2d = snakemake.input.path_start_conc #"data/2-interim/starting_concentration.nc"
path_ghb = snakemake.input.path_ghb_ss_corr #"data/1-external/ghb/ghb_ss_orig_corr.nc"
path_bnd = snakemake.input.path_bnd #"data/2-interim/bnd.nc"
path_ghb_out = snakemake.output.path_ghb_out

# open template
like_2d = xr.open_dataset(path_2d_template)["template"]
like = xr.open_dataset(path_template)["template"]

#open LHM input
ghb_cond_LHM = xr.open_dataset(path_ghb)['conductance'].squeeze("layer", drop=True)
ghb_stage_LHM =xr.open_dataset(path_ghb)['head'].squeeze("layer", drop=True)
ghb_dens_LHM = xr.open_dataset(path_ghb)['density'].squeeze("layer", drop=True)

# open top_bot, bnd, conc2d
layermodel = xr.open_dataset(path_layermodel)
bnd = xr.open_dataset(path_bnd)["ibound"]
conc_2d = xr.open_dataset(path_conc_2d)["sconc_2d"]#.squeeze("band", drop=True)

# assign conc, dens, cond, stage values
ghb_conc_LHM = conc_2d.where(ghb_stage_LHM == 0.0)


# create 3d GHB fron 2d input, check consistency
ghb_sea = create_ghb(
    ghb_stage_LHM,
    ghb_cond_LHM,
    ghb_conc_LHM,
    ghb_dens_LHM,
    like_2d,
    bnd,
    top_bot = layermodel,
    method="z_then_upper",
)

# Create package and write output
#conductance with 6250 to low for LHM-zz layermodel set to 62500
ghb = imod.wq.GeneralHeadBoundary(
    head=ghb_sea["head"],
    conductance=ghb_sea["cond"] *10.0,
    concentration=ghb_sea["conc"],
    density=ghb_sea["dens"],
)
ghb.dataset.to_netcdf(path_ghb_out)