# -*- coding: utf-8 -*-
"""
This script creates the recharge input file by following
the following steps:

1. Open the LHM recharge
2. Convert mm in LHM recharge to meters
3. Check for high values if recharge > 1 mm/day (original max value is 2.3cm/day than adjust to 1 mm/day)
4. Implement recharge of 0.7 mm/day for wadden islands

Input:
- data/1-external/rch/RECHARGE_mean_98_07.IDF
- data/1-external/coastline/wadden_gebied.shp
- data/2-interim/template.nc
- data/2-interim/water_masks.nc

Output:
- data/2-interim/rch.nc
"""
from dask.diagnostics import ProgressBar
import geopandas as gpd
import imod
import numpy as np
import os
from pathlib import Path
import pandas as pd
import xarray as xr

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))
#Paths  
path_Ebs = snakemake.input.path_Ebs
path_qinf = snakemake.input.path_qinf
path_Tact = snakemake.input.path_Tact
path_bdgqmodf = snakemake.input.path_bdgqmodf
path_cell_area = snakemake.input.path_cell_area
path_water = snakemake.input.path_water
path_template_2d = snakemake.input.path_template_2d
path_bnd = snakemake.input.path_bnd
path_bnd_lhm34 = snakemake.input.path_bnd_lhm34
path_rch_out = snakemake.output.path_rch_out

ebs =  xr.open_dataset(path_Ebs)["msw_Ebs"].sel(time=slice('01-01-2011','31-12-2018')) #r"data/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_msw_Ebs.nc"
qinf = xr.open_dataset(path_qinf)["msw_qinf"].sel(time=slice('01-01-2011','31-12-2018')) #r"data/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_msw_qinf.nc"
tact = xr.open_dataset(path_Tact)["msw_Tact"].sel(time=slice('01-01-2011','31-12-2018')) #r"data/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_msw_Tact.nc"
qmodf = xr.open_dataset(path_bdgqmodf)["bdgqmodf"].sel(time=slice('01-01-2011','31-12-2018')) #r"data/1-external/metaswap/run_NHI4.1.1_2009_2018/metaswap_output_2008-2018_bdgqmodf.nc"

metaswap_areaal = imod.idf.open(path_cell_area).squeeze("layer") #r"data/1-external/metaswap/run_NHI4.1.1_2009_2018/msw_Ebs/area_L1.idf"

with ProgressBar():
    p01 = qinf.quantile(0.01)
    p99 = qinf.quantile(0.99)

    # Use boolean inversion to avoid replacing nodata (nan) values.
    qinf = qinf.where(~(qinf < p01), other=p01)
    qinf = qinf.where(~(qinf > p99), other=p99)

    # Units are m per ~10 days (time resolution varies...)    
    groundwater_recharge = qinf + tact + ebs
    groundwater_recharge = groundwater_recharge*(metaswap_areaal/(250.*250.)) #correct for metaswap area
    timedelta = np.full(groundwater_recharge["time"].size, 10.0) #return array of shape groundwater_recharge["time"], filled with 10.0
    timedelta[1:] = groundwater_recharge["time"].diff("time").values / (1.0e9 * 24 * 3600)
    timedelta_days = xr.DataArray(timedelta, {"time": groundwater_recharge["time"]}, ("time",)) 
    groundwater_recharge = groundwater_recharge.assign_coords(timedelta=("time", timedelta_days))
    groundwater_recharge["timedelta"].attrs["units"] = "days"
    groundwater_recharge.attrs["units"] = "m"
    groundwater_recharge.name = "groundwater_recharge"
    groundwater_recharge = groundwater_recharge.astype(np.float32)
    #groundwater_recharge.to_netcdf("metaSWAP_2011-2018_groundwater_recharge_NL_m.nc", encoding={"groundwater_recharge": {"zlib": True}})

# Sum, since it's data per 9 or 10 days.
frequency= "D"
flux = groundwater_recharge.resample(time=frequency).sum("time")
duration = (groundwater_recharge["timedelta"] / 1).resample(time=frequency).sum("time")

# Now compute m/d
rch = flux / duration
rch_mean = rch.mean(dim= 'time').squeeze('layer', drop=True)

#open template
like_2d = xr.open_dataset(path_template_2d)['template'] #r"data/2-interim/template_2d.nc"
bnd_old_l1 = imod.idf.open(path_bnd_lhm34).squeeze('layer',drop=True) #"data/1-external/bnd/lhm34/ibound_l1.idf"
bnd_old_l1 = imod.prepare.Regridder(method="mean").regrid(bnd_old_l1, like_2d)
water_masks = xr.open_dataset(path_water) #r"data/2-interim/water_masks.nc"
sea_2d = water_masks['sea_2d']
water = water_masks['water_2d']



#check for high values if recharge > 1 mm/day (original max value is 2.3cm/day than adjust to 1 mm/day)
groundwater_recharge_average = rch_mean.where(rch_mean <0.001, 0.001)
groundwater_recharge_average = imod.prepare.Regridder(method="mean").regrid(groundwater_recharge_average, like_2d)
imod.idf.write(Path(path_Ebs).parent / 'groundwater_recharge_average_2011-2018_md.idf',groundwater_recharge_average )
bnd = xr.open_dataset(path_bnd)['ibound'] #r"data/2-interim/bnd.nc"

#get 2d first active domain
da = imod.select.layers.upper_active_layer(bnd)
active = bnd.layer == da
active_2d = active.max(dim = 'layer')

# Create package and write to file
rch = imod.wq.RechargeHighestActive(rate=groundwater_recharge_average, concentration=0.05)
rch.dataset.to_netcdf(path_rch_out)

