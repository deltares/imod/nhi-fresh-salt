"""
This script creates input for the KP SeaLevelRise scenario
- For both LHM-fs and LHM? Yes! (but first LHM-fs)

The scenario:
- Sea level rise in the Sea (GHB) and open connected water bodies:
  - Rhine-Meuse delta, including Haringvliet. SLR according to calculations of De Jong for median river discharge, linearly extrapolated (RIV-H)
  - Ooster- and Westerschelde, level with SLR (RIV-H)
- Transient SLR curve as supplied by RWS. Up until maximum SLR of 3m (~2300 AD)
--> check for flooding? No, assume policy as is

- Soil subsidence:
  - to be decided. 
  - Regional water levels go down with subsidence, as well as the surface elevation. --> Ook kD? check met Joachim.
Uitgangspunten implementatie bodemdaling in Deltascenario's:
"De bodemdaling is in het model geïmplementeerd vertaald naar de peilen van het oppervlaktewater en het maaiveld.
 De oppervlaktewaterpeilen van het regionaal systeem zijn aangepast waarbij is aangenomen dat de ontwateringsdiepte 
 gelijk blijft. Er is aangenomen dat de peilen van het hoofdwatersysteem niet beïnvloed worden door de bodemdaling. 
 Hierom zijn deze peilen niet aangepast als gevolg van de bodemdaling."
 Verder: in Pleistoceen NL bodemdaling (beekdalen) gereduceerd tot max 1 cm/j; voor natte natuur (Landgebruik 13) 
 dalingssnelheid met 50% gereduceerd"
  - ==> is maaiveld echt belangrijk om mee te laten dalen? Of alleen voor ghg plaatjes? Wel richting LHM

- Recharge?
  - remains at present amount

- Changes in salinity of surface water?
  - RMM no? <> Ymkje
  - Ooster/Westerschelde: no


Input:

Output:

"""
import numpy as np
import pandas as pd
import xarray as xr
from pathlib import Path
import cftime
import imod
import gc
from dask.diagnostics import ProgressBar

#### inputs

path_interim = Path("d:/nhi-fresh-salt/data/2-interim")
path_bas = path_interim / "bas.nc"

# sea and main waterways
path_ghb = path_interim / "ghb_kpslr.nc"
path_rivh = path_interim / "river_h_kpslr.nc"
path_rivh_drn = path_interim / "river_h_drn_kpslr.nc"
# regional rivers
path_rivp = path_interim / "river_p_kpslr.nc"
path_rivp_drn = path_interim / "river_p_drn_kpslr.nc"
path_rivs = path_interim / "river_s_kpslr.nc"
path_rivs_drn = path_interim / "river_s_drn_kpslr.nc"
path_rivt = path_interim / "river_t_kpslr.nc"
path_drn_b = path_interim / "drainage_b_kpslr.nc"
path_drn_mvgrep = path_interim / "drainage_mvgrep_kpslr.nc"
path_drn_sof = path_interim / "drainage_sof_kpslr.nc"
path_drn_boils = path_interim / "boils_kpslr.nc"
"""
path_bas = snakemake.input.path_bas

# sea and main waterways
path_ghb = Path(snakemake.input.path_ghb)
path_rivh = Path(snakemake.input.path_rivh)
path_rivh_drn = Path(snakemake.input.path_rivh_drn)
path_rivhslr = Path(snakemake.input.path_rivhslr) # ==> from prepare_kpslr
# # regional rivers
path_rivp = Path(snakemake.input.path_rivp)
path_rivp_drn = Path(snakemake.input.path_rivp_drn)
path_rivs = Path(snakemake.input.path_rivs)
path_rivs_drn = Path(snakemake.input.path_rivs_drn)
path_rivt = Path(snakemake.input.path_rivt)
path_drn_b = Path(snakemake.input.path_drn_b)
path_drn_mvgrep = Path(snakemake.input.path_drn_mvgrep)
path_drn_sof = Path(snakemake.input.path_drn_sof)
path_drn_boils = Path(snakemake.input.path_drn_boils)
path_interim = Path(snakemake.params.path_interim)
"""
#####
# remove DRNs or other boundconds that cause problems at higher SLRs
# Use final timestep as probable worst case
# --> merge layers 1-9 and 11-15 into one layer

bas = imod.wq.BasicFlow.from_file(path_bas)
thick = bas.thickness()
thick = thick.sel(layer=slice(1,9)).sum(dim="layer")
thick = thick.assign_coords(layer=1)
top_bot = xr.Dataset()
top_bot["dz"] = thick.where(bas["ibound"].sel(layer=10)==1)

def _make_check(pkg):
    # select last timestep
    # reduce layers
    pkg = pkg.isel(time=-1)
    vars = list(pkg.keys())
    pkg = pkg.drop_vars([v for v in vars if v not in ["conductance","stage","head","elevation","bottom_elevation"]])
    pkg_l1 = pkg.sel(layer=slice(1,9))
    if pkg_l1.dims["layer"]:
        pkg_l1 = pkg_l1.max(dim="layer")
        return pkg_l1.assign_coords(layer=1)
    else:
        return None

ghb = {"ghb":_make_check(xr.open_dataset(path_ghb))}
paths_riv = {"rivh":path_rivh,"rivp":path_rivp, "rivs":path_rivs, "rivt":path_rivt}
riv = {k:_make_check(xr.open_dataset(v)) for k,v in paths_riv.items()}
paths_drn = {"rivdrnh":path_rivh_drn,"rivdrnp":path_rivp_drn, "rivdrns":path_rivs_drn, "drnb":path_drn_b, "drnmvgrep":path_drn_mvgrep, "drnsof":path_drn_sof}
drn = {k:_make_check(xr.open_dataset(v)) for k,v in paths_drn.items()}

dt_min, dt_all = imod.evaluate.intra_cell_boundary_conditions(top_bot, ghb=ghb, riv=riv, drn=drn)

small_dt = dt_all.where(dt_all < 50.)
for dim in small_dt.dims:
    small_dt = small_dt.dropna(dim=dim,how="all")
small_dt.name = "small_dt"
small_dt = small_dt.drop(["dx","dy"])
small_dt = small_dt.to_dataset()
#small_dt.to_netcdf("data/tmp/small_dt.nc")
#small_dt = xr.open_dataset("data/tmp/small_dt.nc",chunks={"combination":1,"layer":1})["small_dt"]
gc.collect()
small_dt_df = small_dt.to_dataframe().dropna()

small_dt_df = small_dt_df.reset_index()
small_dt_df["source"] = small_dt_df.combination.apply(lambda x:x.split("-")[0])
small_dt_df["sink"] = small_dt_df.combination.apply(lambda x:x.split("-")[1])

small_dt_df = small_dt_df.reindex(columns=["x","y","z","layer","combination","source","sink","small_dt"])
small_dt_df.to_csv(path_interim / "small_dt_boundarycond_kpslr.csv")
imod.ipf.write(path_interim / "small_dt_boundarycond_kpslr.ipf", small_dt_df)

# duplicate small_dt_df to all 9 layers
dfl = []
for l in range(1,10):
    dfl.append(small_dt_df.copy())
    dfl[-1]["layer"] = l
small_dt_df = pd.concat(dfl, axis=0, ignore_index=True)

# as points_set_values fils on missing layer
def _reindex(ds):
    coords = dict(x=ds.x,y=ds.y)
    if "layer" in ds.dims:
        coords["layer"] = np.arange(float(ds.coords["layer"].min()),float(ds.coords["layer"].max())+1.)
    ds = ds.assign_coords({"dlayer":1})
    return ds.reindex(coords)

def _dataset_set_points_values(ds, values, **coords):
    #print(ds)
    ds = _reindex(ds)
    #coords["layer"] = coords["layer"].astype(float) + 0.5
    coords = {k:v for k,v in coords.items() if k in ds.dims}
    b = imod.select.points_in_bounds(ds, **coords)
    coords = {k:v[b] for k,v in coords.items()}

    for dvar in ds.data_vars:
        ds[dvar] = ds[dvar].load()
        selcoords = {k:v for k,v in coords.items() if k in ds[dvar].dims}
        ds[dvar] = imod.select.points_set_values(ds[dvar], values, **selcoords)
        #try:
        #    ds[dvar] = values
        #except ValueError:
        #    print("Valueerror?")
        #    pass
    return ds

ghb = {"ghb":xr.open_dataset(path_ghb)}
riv = {k:xr.open_dataset(v) for k,v in paths_riv.items()}
drn = {k:xr.open_dataset(v) for k,v in paths_drn.items()}

corrected = []
# throw out sink, not source
for c in small_dt_df.sink.unique():
    print(c)
    sel = small_dt_df.loc[small_dt_df.sink == c]

    if "drn" in c:
        drn[c] = _dataset_set_points_values(drn[c], np.nan, x=sel.x.values, y=sel.y.values, layer=sel.layer.values)
    elif "riv" in c:
        riv[c] = _dataset_set_points_values(riv[c], np.nan, x=sel.x.values, y=sel.y.values, layer=sel.layer.values)
    elif "ghb" in c:
        ghb[c] = _dataset_set_points_values(ghb[c], np.nan, x=sel.x.values, y=sel.y.values, layer=sel.layer.values)

    corrected.append(c)
    gc.collect()

def _append_corr(path):
    path = Path(path)
    if "_corr" not in str(path):
        path = path.parent / f"{path.stem}_corr{path.suffix}"
    else:
        path = path.parent / f"{path.stem}2{path.suffix}"
    return path

# save corrected
for c in corrected:
    if "riv" in c:
        fn = _append_corr(paths_riv[c])
        riv[c].to_netcdf(fn)
    elif "ghb" in c:
        fn = _append_corr(path_ghb)
        ghb[c].to_netcdf(fn)
    elif "drn" in c:
        fn = _append_corr(paths_drn[c])
        drn[c].to_netcdf(fn)
    print(f"Saved corrected {c} to {str(fn)}")
