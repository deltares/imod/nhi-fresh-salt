# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 17:24:02 2019

@author: mulde_ts
"""
import os

import imod
import numba
import numpy as np
import pandas as pd
import xarray as xr
import re
from pathlib import Path


os.chdir(r'c:\Users\mulde_ts\projects\nhi-fresh-salt')   #not local : change to p:\11203718-007-nhi-zoetzout\NHI_zz_basic 

# path_bnd = r"data/2-interim/bnd.nc"
wel_path = Path("data/1-external/wel")
well_screen_minimum = 5.0  # m
well_screen_maximum = 20.0  # m

def open_top():
    # Deal with names...
    # Aquifer top and bottoms are a superset of aquitard top and bottom!
    # due to: surface level and geohydrological base
    top_l1 = imod.idf.open(
        "data/1-external/LHM34_lagenmodel/AHN_F250.idf"
    ).assign_coords(layer=1)

    top = imod.idf.open(
        "data/1-external/LHM34_lagenmodel/BOT_SDL*.idf", pattern="{name}_SDL{layer}_M"
    )

    top.layer.values += 1
    return xr.concat([top_l1, top], dim="layer").transpose("layer", "y", "x")


def open_bot():
    # Deal with names...
    bot = imod.idf.open(
        "data/1-external/LHM34_lagenmodel/TOP_SDL*.idf", pattern="{name}_SDL{layer}_M"
    )

    bot_l7 = imod.idf.open(
        "data/1-external/LHM34_lagenmodel/TOP_GEOHYDROLOGISCHE_BASIS.IDF"
    ).assign_coords(layer=7)

    return xr.concat([bot, bot_l7], dim="layer").transpose("layer", "y", "x")


@numba.njit
def _overlap(a, b):
    return max(0, min(a[1], b[1]) - max(a[0], b[0])) 

@numba.njit
def Q_per_layer(filtertop, filterbot, layertop, layerbot, Q, kh):
    nlayer = layertop.size
    layer = np.arange(1, nlayer + 1)
    overlaps = np.zeros(nlayer)
    total_overlap = 0.0
    kd_total = 0.0
    for i in range(nlayer):
        if np.isnan(kh[i]):
            continue
        overlap = _overlap((filterbot, filtertop), (layerbot[i], layertop[i]))
        if np.isnan(overlap):
            overlap = 0.0
        overlaps[i] = overlap
        total_overlap += overlap
        kd_total += kh[i] * overlap
    
    kd_effective = overlaps * kh
    proportion = kd_effective / kd_total
    Qlayer = Q * proportion
    missing = (Qlayer >= 0.0) | np.isnan(Qlayer)    
#    missing = (Qlayer == 0.0) | np.isnan(Qlayer)
    has_value = ~missing
    Qlayer = Qlayer[has_value]
    layer = layer[has_value]
    return Qlayer, layer



@numba.njit
def _place_wells(x, y, filtertop, filterbot, Q, layertop, layerbot, kh):
    npoints, nlayer = layertop.shape
    n_allocate = nlayer * npoints
    xs = np.zeros(n_allocate)
    ys = np.zeros(n_allocate)
    Qlayers = np.zeros(n_allocate)
    layers = np.zeros(n_allocate, dtype=np.int32)
    valid = np.zeros(n_allocate, dtype=np.bool_)
    
    count = 0
    for i in range(npoints):
        print(i)
        t = layertop[i]
        b = layerbot[i]
        if np.isnan(t).all():
            continue
        if np.isnan(b).all():
            continue        
        Qlayer, layer = Q_per_layer(filtertop[i], filterbot[i], t, b, Q[i], kh[i])
        layersize = layer.size
        xs[count: count + layersize] = x[i]
        ys[count: count + layersize] = y[i]
        Qlayers[count: count + layersize] = Qlayer
        layers[count: count + layersize] = layer
        valid[count: count + layersize] = True
        count += nlayer

    return xs, ys, Qlayers, layers, valid


def place_wells(idcode, x, y, filtertop, filterbot, Q, layertop, layerbot, kh):
    nlayer, _, _ = layertop.shape
    indices = imod.select.points_indices(layertop, x=x, y=y)
    tops = layertop.values[:, indices["y"], indices["x"]].T
    bots = layerbot.values[:, indices["y"], indices["x"]].T
    khs = kh.values[:, indices["y"], indices["x"]].T
    #tops = layertop.isel(**indices).transpose("index", "layer")
    #bots = layerbot.isel(**indices).transpose("index", "layer")
#    khs = kh.isel(**indices).transpose("index", "layer")
    #print(tops)

    new_x, new_y, new_Q, new_layer, valid = _place_wells(
        x.values,
        y.values,
        filtertop.values,
        filterbot.values,
        Q.values,
        tops,#.values,
        bots,#.values,
        khs,#.values,
    )
    df = pd.DataFrame()
    df["id"] = np.repeat(idcode.values, nlayer)[valid]
    df["x"] = new_x[valid]
    df["y"] = new_y[valid]
    df["Q"] = new_Q[valid]
    df["layer"] = new_layer[valid]
    return df
    
path_top_bot = r"data\2-interim\top_bot_1mdik.nc"
top_bot_40layer = xr.open_dataset(path_top_bot)
top_bot = top_bot_40layer



top = open_top()
bot = open_bot()

path_conductivity  = "data/2-interim/conductivity.nc"
conductivity = xr.open_dataset(path_conductivity)
kh = conductivity["kh"]
porosity = 0.3

bdgfrf = imod.idf.open_subdomains(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\4-output\output-V06_1mdik\bdgfrf\bdgfrf_*.idf').compute()
bdgfrf = bdgfrf.squeeze(dim = 'time', drop =True)
bdgflf = imod.idf.open_subdomains(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\4-output\output-V06_1mdik\bdgflf\bdgflf_*.idf').compute()
bdgflf = bdgflf.squeeze(dim = 'time', drop =True)
bdgfff = imod.idf.open_subdomains(r'p:\11203718-007-nhi-zoetzout\NHI_zz_basic\data\4-output\output-V06_1mdik\bdgfff\bdgfff_*.idf').compute()
bdgfff = bdgfff.squeeze(dim = 'time', drop =True)
def stability_criterion_advection(bdgfrf, bdgfff, bdgflf, top_bot, porosity):
    R = 1. # no sorption: retardation factor is 1

    if "dz" not in top_bot:
        top_bot["dz"] = top_bot["top"] - top_bot["bot"]
    # dz between layers is 0.5*dz_up + 0.5*dz_down
    dz_m = top_bot.dz.rolling(layer=2,min_periods=2).mean()
    dz_m = dz_m.shift(layer=-1)

    # cell side area (m2)
    A_x = top_bot.dz * top_bot.dy
    A_y = top_bot.dz * top_bot.dx
    A_z = abs(top_bot.dx * top_bot.dy)

    # specific discharge (m/d)
    qs_x = bdgfrf / A_x
    qs_y = bdgfff / A_y
    qs_z = bdgflf / A_z

    # absolute velocities (m/d)
    abs_v_x = xr.apply_ufunc(np.abs, qs_x / porosity, dask="allowed")
    abs_v_y = xr.apply_ufunc(np.abs, qs_y / porosity, dask="allowed")
    abs_v_z = xr.apply_ufunc(np.abs, qs_z / porosity, dask="allowed")

    # dt of constituents (d)
    dt_x = R / (abs_v_x / top_bot.dx)
    dt_y = R / (abs_v_y / abs(top_bot.dy))
    dt_z = R / (abs_v_z / dz_m)

    # overall dt due to advection criterion (d)
    dt = 1. / (1. / dt_x + 1. / dt_z + 1. / dt_z )

    return dt, dt_x, dt_y, dt_z


test_stability_adv = stability_criterion_advection(bdgfrf, bdgfff, bdgflf, top_bot, porosity)
dt = test_stability_adv[0]
dt_x = test_stability_adv[1]
dt_y = test_stability_adv[2]
dt_z = test_stability_adv[3]


print(dt.min())
print(dt_x.min())
print(dt_y.min())
print(dt_z.min())


# read all wel files
# do not use glob function of imod.ipf.read to store original filenames
# either:
# - ipf has filter information: use as top and bot, let imod-seawat assign layer, or:
# - ipf only has LHM layer information, set top and bot to top and bot of original LHM layer
dfs = []
pattern = re.compile(r".+_[lL]([0-9]+).*")
for fn in wel_path.glob("*.ipf"):
    df = imod.ipf.read(fn)
    df["fn_orig"] = fn.stem
    m = pattern.match(fn.stem)
    if m:
        df["fn_layer"] = int(m[1])
    else:
        df["fn_layer"] = None
    dfs.append(df)
df = pd.concat(dfs, axis=0, ignore_index=True, sort=False)



# merge different column names for the same column
merge = {
    "x": ["xcoord"],
    "y": ["ycoord"],
    "q": ["Q_ASSIGNED", "value"],
    # "layer": ["lay", "laag", "fn_layer"],  # for layer: assure it is taken from filename
    "top_filt": ["zmax"],
    "bot_filt": ["zmin"],
}
for first, others in merge.items():
    for other in others:
        df[first] = df[first].combine_first(df[other])

# conform order to imod-seawat
df = df.reindex(columns=["x", "y", "q", "top_filt", "bot_filt", "fn_layer", "fn_orig"])

# conform names to imod-seawat
df = df.rename(
    columns={
        "x": "X",
        "y": "Y",
        "q": "Q",
        "top_filt": "TOP",
        "bot_filt": "BOT",
        "fn_layer": "LHMfresh_layer",
        "fn_orig": "LHMfresh_fn",
    }
)

# select top and bot of top_bot_orig where layer code is present
df_sel = df.loc[~df["LHMfresh_layer"].isnull()]
idx = imod.select.points_indices(
    top, x=df_sel["X"], y=df_sel["Y"], layer=df_sel["LHMfresh_layer"]
)
laytop = top.isel(**idx)
laybot = bot.isel(**idx)
laytop = laytop.drop(("dx", "dy", "x", "y", "layer")).to_series()
laybot = laybot.drop(("dx", "dy", "x", "y", "layer")).to_series()
df.loc[df_sel.index, "laytop"] = laytop.values  # .values b/c index is not aligned
df.loc[df_sel.index, "laybot"] = laybot.values

# apply minimal and maximum well screen length, from laytop 
# (assume filt_top/bot in original files are correct)
screenlength = df["laytop"] - df["laybot"]
screenlength = screenlength.where(
    screenlength > well_screen_minimum, well_screen_minimum
)
screenlength = screenlength.where(
    screenlength < well_screen_maximum, well_screen_maximum
)
df["laybot"] = df["laytop"] - screenlength

# and merge with TOP and BOT column
df["topbot_from_layer"] = 0
df["topbot_from_layer"] = df["topbot_from_layer"].where(~df["TOP"].isnull(), 1)
df["TOP"] = df["TOP"].combine_first(df["laytop"])
df["BOT"] = df["BOT"].combine_first(df["laybot"])
Q = df['Q']
filtertop = df["TOP"]
filterbot = df["BOT"]

df["idcode"] =  df.index.map(str)
df["filtertop"] = df["TOP"]
df["filterbot"] = df["BOT"]

df2 = df.copy()
df = df2#.iloc[:1000, :]

layertop=top_bot_40layer["top"].compute()
layerbot=top_bot_40layer["bot"].compute()
out_df = place_wells(
    idcode=df["idcode"],
    x=df["X"],
    y=df["Y"],
    filtertop=df["filtertop"],
    filterbot=df["filterbot"],
    Q=df["Q"],
    layertop=layertop,
    layerbot=layerbot,
    kh=kh,
)
porosity = 0.3
wel = out_df
top_bot = top_bot_40layer

def stability_criterion_wel(wel, top_bot, porosity):
    # assumes X, Y and layer present in WEL
    
    R = 1. # no sorption: retardation factor is 1

    b = imod.select.points_in_bounds(top_bot, x=wel["x"],y=wel["y"],layer=wel["layer"])
    indices = imod.select.points_indices(top_bot, x=wel.loc[b,"x"],y=wel.loc[b,"y"],layer=wel.loc[b,"layer"])

    top_bot["dz"] = top_bot["top"] - top_bot["bot"]
    top_bot["volume"] = top_bot["dz"] * top_bot.dx * -top_bot.dy

    wel.loc[b,"qs"] = wel.loc[b,"Q"].abs() / top_bot["volume"].isel(indices).values
    wel.loc[b,"dt"] = R * porosity / wel.loc[b,"qs"]

    return wel

test_stability =  stability_criterion_wel(wel,top_bot,porosity)
print(test_stability.qs.min(),test_stability.qs.max(), test_stability.dt.min(),test_stability.dt.max())

wells = out_df
Q_correction = (test_stability['Q']*test_stability['dt'])/100
wells['Q'] = Q_correction.where(test_stability['dt'] <= 100, test_stability['Q'] )

wel =wells
test_stability =  stability_criterion_wel(wel,top_bot,porosity)
print(test_stability.qs.min(),test_stability.qs.max(), test_stability.dt.min(),test_stability.dt.max())




out_df.to_csv('negativwells_per_layer.csv')

lay_with_well = out_df.layer.unique()
for item in lay_with_well:
    sep_layer = out_df.loc[out_df['layer'] == item ]
#    sep_layer.to_csv('wel_L%s.ipf' %item) 
    imod.ipf.write(('negativ_wel_L%s.ipf' %item), sep_layer.round(3))