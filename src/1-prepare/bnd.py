# -*- coding: utf-8 -*-
"""
This script creates the ibound and bas package, 
by following the following steps:
1. define ibound from cells with kh/kv information
2. remove isolated cells from ibound
3. store in bnd.nc
4. create bas package and store in bas.nc

This assumes that the actual definition of the 
model extent is done in layermodel.py.

Input:
- data/2-interim/layermodel.nc

Output:
- data/2-interim/bnd.nc

"""
import scipy.ndimage
import numpy as np
import os
import xarray as xr

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# open paths
path_layermodel = snakemake.input.path_layermodel #"data/2-interim/layermodel.nc"
path_bnd_out = snakemake.output.path_bnd_out

# load layermodel
layermodel = xr.open_dataset(path_layermodel)

# create ibound from layermodel
bnd = layermodel["ibound"]
bnd.name = "ibound"  # rename to ibound

def inactivate_isolated_cells(bnd, cutoff=1e-2):
    """ Function that inactivates ibound areas that are 
    smaller than the cutoff (in fraction of number of cells)
    """
    active_cells = bnd != 0
    label, _ = scipy.ndimage.label(active_cells)
    unique, counts = np.unique(label, return_counts=True)
    counts = counts / counts.sum()
    if (counts < cutoff).any():  # less than cutoff: unconnected region
        isolated = unique[counts < cutoff]
        isolated = np.isin(label, isolated)
        bnd = bnd.where(~isolated, 0)  # set bnd to zero for isolated regions
    return bnd
bnd = inactivate_isolated_cells(bnd)

# write ibound
bnd.to_netcdf(path_bnd_out)
