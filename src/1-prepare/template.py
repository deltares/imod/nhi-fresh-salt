"""
Creates 3d and 2d template files, by following
the following steps:
1. read number of layers from bnd
2. set fixed extent

Input:
- data/2-interim/bnd.nc

Output:
- data/2-interim/template.nc
- data/2-interim/template_2d.nc
"""

import numpy as np
import os
import xarray as xr

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__),"..",".."))

path_ibound = snakemake.input.path_ibound
path_tmpl_out = snakemake.output.path_tmpl_out
path_tmpl2d_out = snakemake.output.path_tmpl2d_out

bnd  = xr.open_dataset(path_ibound)['ibound'] #r'data/2-interim/bnd.nc'
# fixed coordinates
xmin = 0
xmax = 300000
ymin = 300000
ymax = 625000
dy = -250
dx = 250

dims = ("layer", "y", "x")

coords = {
    "layer": bnd['layer'],
    "y": np.arange(ymax, ymin, dy) + 0.5 * dy,
    "x": np.arange(xmin, xmax, dx) + 0.5 * dx,
    "dx":dx,
    "dy":dy
}

nrow = coords["y"].size
ncol = coords["x"].size
nlay = coords["layer"].size

like = xr.DataArray(np.full((nlay, nrow, ncol), np.nan), coords, dims)
like_2d = like.isel(layer=0).drop(["layer"])

like.name = "template"
like_2d.name = "template"
like.to_netcdf(path_tmpl_out)
like_2d.to_netcdf(path_tmpl2d_out)