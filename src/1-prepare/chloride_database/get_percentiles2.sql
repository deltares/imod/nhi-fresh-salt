explain analyze 
select l.locationcode, l.geometrypoint, l.x, l.y, d.z, d.p10, d.p25, d.p40, d.p50, d.p60, d.p75, d.p90
from (
	select d10.locationid, d10.z, d10.value as p10, d25.value as p25, d40.value as p40, d50.value as p50, d60.value as p60, d75.value as p75, d90.value as p90
	from dataseries d10
	join dataseries d25 on d25.locationid = d10.locationid and d25.z = d10.z
	join dataseries d40 on d40.locationid = d10.locationid and d40.z = d10.z
	join dataseries d50 on d50.locationid = d10.locationid and d50.z = d10.z
	join dataseries d60 on d60.locationid = d10.locationid and d60.z = d10.z
	join dataseries d75 on d75.locationid = d10.locationid and d75.z = d10.z
	join dataseries d90 on d90.locationid = d10.locationid and d90.z = d10.z
	where d10.parameterid = 6 and 
		d25.parameterid = 7 and 
		d40.parameterid = 12 and 
		d50.parameterid = 8 and 
		d60.parameterid = 13 and 
		d75.parameterid = 9 and 
		d90.parameterid = 10 
	) d
	join location l on l.locationid = d.locationid
	limit 10000