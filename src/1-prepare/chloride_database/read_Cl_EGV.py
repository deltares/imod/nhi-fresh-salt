"""
Read Arcadis data and merge in a single file.

Script goes through the different folders and reads the files with the termination 'Cl_db'.
Old versions of the original Arcadis data are in folders called '_old'. These are not read by the script.

"""

basepath = r'p:\11203718-007-nhi-zoetzout\Gegevensuitvraag\Producten\Grondwater'
resultfile = 'GW_Results_Cl_v2'
parameter_id = 'Cl_db'     # Chloride = 'Cl_db', EC = 'EGV.csv'

import os, glob, pandas
from pandas.io.parsers import ParserError

df = pandas.DataFrame([])
dir_list = []
for root, dirs, files in os.walk(basepath):
    for f in files:
        dir_list.append(os.path.join(root,f))

for key, value in enumerate(dir_list):
    if parameter_id in value and '_old' not in value:
        with open(value,'r') as f:
            file = f.read().replace('"','').replace('Date','date').replace('Value','value')
            w = open(os.path.join(basepath,'file.csv'),'w')
            w.write(file)
            w.close()
            try:
                csv = pandas.read_csv(os.path.join(basepath,'file.csv'), sep='\t|,', engine='python')
                if 'Provincie' not in csv.columns:
                    csv['Provincie'] = dir_list[key].split('\\')[-2].replace('_',' ') 
            except ParserError:
                with open(os.path.join(basepath,'file.csv'),'r') as f:
                    file = f.read().replace(',','.')
                    w = open(os.path.join(basepath,'file2.csv'),'w')
                    w.write(file)
                    w.close()
                csv = pandas.read_csv(os.path.join(basepath,'file2.csv'), sep='\t|,', engine='python')
                os.remove(os.path.join(basepath,'file2.csv'))
            df = pandas.concat([df,csv], axis=0, ignore_index=True, sort=False)

os.remove(os.path.join(basepath,'file.csv'))
df = df.rename(columns={"Provincie": "Owner"})
df.to_csv(os.path.join(basepath,str(resultfile)+'.csv'))
df.to_pickle(os.path.join(basepath,str(resultfile)+'.pickle'))
