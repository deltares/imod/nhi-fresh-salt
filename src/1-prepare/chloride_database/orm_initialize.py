"""
Creates ORM model for zoetzout data from various sources.
"""

#  Copyright notice
#   --------------------------------------------------------------------
#   Copyright (C) 2019 Deltares for LHM Zoetzout
#       Gerrit Hendriksen@deltares.nl
#
#   This library is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this library.  If not, see <http://www.gnu.org/licenses/>.
#   --------------------------------------------------------------------
#
# This tool is part of <a href="http://www.OpenEarth.eu">OpenEarthTools</a>.
# OpenEarthTools is an online collaboration to share and manage data and
# programming tools in an open source, version controlled environment.
# Sign up to recieve regular updates of this function, and to contribute
# your own tools.

# $Id: orm_hydrodb.py 945 2016-09-08 14:35:49Z hendrik_gt $
# $Date: 2016-09-08 16:35:49 +0200 (Thu, 08 Sep 2016) $
# $Author: hendrik_gt $
# $Revision: 945 $
# $HeadURL: https://repos.deltares.nl/repos/NHI/trunk/engines/HMDB/orm_hydrodb.py $
# $Keywords: $
import configparser
from sqlalchemy import create_engine,MetaData
from sqlalchemy.orm import sessionmaker

# Declare a Mapping to the database
from orm_zoetzout import Base, Compartment, Unit, Parameter, Type


"""some functions"""
def read_config(af):
	# Default config file (relative path, does not work on production, weird)
	# Parse and load
    cf = configparser.ConfigParser() 
    cf.read(af)
    return cf

def getConnStrFromFile(connFile):
        connFile = open(connFile, 'r')
        connStr = connFile.read()
        connFile.close()
        return connStr
    
local = False
if local:
    cf = read_config(r'credentials_template.txt')
else:
    cf = read_config(r'credentials_zoetzout_remote.cfg')
connstr = 'postgres://'+cf.get('Postgis','user')+':'+cf.get('Postgis','pwd')+'@'+cf.get('Postgis','host')+':5432/'+cf.get('Postgis','dbname')
engine = create_engine(connstr,echo=True)

meta=MetaData(engine)

# function to create the database, bear in mind, only to be executed when first started
## Create the Table in the Database
Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)


# some prepatory work, adding constants to the lookuptables/domaintables
# in most cases used once
def filldomaintables(session):
    c = Compartment('OW','Oppervlaktwater')
    session.add(c)
    c = Compartment('GW','Grondwater')
    session.add(c)
    
    u = Unit('Grams per liter','g/L')
    session.add(u)
    u = Unit('Milligrams per liter','mg/L',0.001)
    session.add(u)
    u = Unit('Meter','m')
    session.add(u)
    u = Unit('Celsius','oC')
    session.add(u)
    u = Unit('MicroSiemens per centimeter','uS/cm')
    session.add(u)
    u = Unit('Siemens per meter','S/m',10.)
    session.add(u)
    u = Unit('Kilograms per cubic meter','kg/m3')
    session.add(u)
    u = Unit('Ohm-meter','Ohmm')
    session.add(u)
    
    #complete with Parameter and Type 
    t = Type('ANA','Chemical analysis')
    session.add(t)
    t = Type('VES','Vertical Electrical Sounding')
    session.add(t)
    t = Type('ECPT','Electrical Cone Penetration Test')
    session.add(t)
    t = Type('BL','Borehole logging')
    session.add(t)
    t = Type('SF','SlimFlex measurement')
    session.add(t)
    t = Type('TEC','TEC probe measurement')
    session.add(t)
    t = Type('ZW','Zoutwachter measurement')
    session.add(t)
    t = Type('AEM','Airborne EM measurement')
    session.add(t)
    t = Type('FHEM','FRESHEM Airborne EM measurement')
    session.add(t)
    t = Type('SOFT_SEA','Soft data point for interpolation - Sea')
    session.add(t)
    t = Type('SOFT_DEEP','Soft data point for interpolation - Deep')
    session.add(t)
    t = Type('SOFT_OTH','Soft data point for interpolation - Other')
    session.add(t)
    t = Type('EC','Electrical Conductivity measurement')
    session.add(t)
    t = Type('SOFT_FRESHBRACK','Soft data point for interpolation - Fresh-brackish interface')
    session.add(t)
    t = Type('SOFT_BRACKSALT','Soft data point for interpolation - Brackish-salt interface')
    session.add(t)
    t = Type('SOFT_TRANSGR','Soft data point for interpolation - Transgression')
    session.add(t)
    t = Type('SOFT_CONNATE','Soft data point for interpolation - Connate')
    session.add(t)


    p = Parameter('Cl','Chloride')
    session.add(p)
    p = Parameter('ECw','Electrical conductivity water')
    session.add(p)
    p = Parameter('ECb','Electrical conductivity bulk')
    session.add(p)
    p = Parameter('R','Resistivity')
    session.add(p)
    p = Parameter('Cl_p5','Chloride - 5 percentile value')
    session.add(p)
    p = Parameter('Cl_p10','Chloride - 10 percentile value')
    session.add(p)
    p = Parameter('Cl_p25','Chloride - 25 percentile value')
    session.add(p)
    p = Parameter('Cl_p50','Chloride - 50 percentile value')
    session.add(p)
    p = Parameter('Cl_p75','Chloride - 75 percentile value')
    session.add(p)
    p = Parameter('Cl_p90','Chloride - 90 percentile value')
    session.add(p)
    p = Parameter('Cl_p95','Chloride - 95 percentile value')
    session.add(p)
    p = Parameter('Cl_p40','Chloride - 40 percentile value')
    session.add(p)
    p = Parameter('Cl_p60','Chloride - 60 percentile value')
    session.add(p)
    session.commit()

# create session
Session = sessionmaker(bind = engine)
session = Session()

filldomaintables(session)         #execute only if necessary
session.close()