import configparser
from pathlib import Path
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, MetaData, func
import matplotlib.pyplot as plt

# local
from orm_zoetzout import Base, Location, Dataseries, Compartment, Unit, Parameter, Type

basedir = Path(r"c:\Svnrepos\nhi-fresh-salt")
chloridedbdir = basedir / "src/1-prepare/chloride_database"

"""some functions"""
def read_config(af):
    # Default config file (relative path, does not work on production, weird)
    # Parse and load
    cf = configparser.ConfigParser()
    cf.read(af)
    return cf

# setup connection to database
local = False
if local:
    cf = read_config(chloridedbdir / "credentials_zoetzout_local.cfg")
else:
    cf = read_config(chloridedbdir / "credentials_zoetzout_remote.cfg")
connstr = (
    "postgres://"
    + cf.get("Postgis", "user")
    + ":"
    + cf.get("Postgis", "pwd")
    + "@"
    + cf.get("Postgis", "host")
    + ":5432/"
    + cf.get("Postgis", "dbname")
)
engine = create_engine(connstr, echo=True)

meta = MetaData(engine)

# create session --> all initialisation work has been done in orm_initialize
Session = sessionmaker(bind=engine)
session = Session()

# read the query from "compare_datatypes.sql"
with open(chloridedbdir / "compare_datatypes.sql", "r") as f:
    sql = f.read()

# create typeid -> typecode series
type_ = pd.read_sql_table("type",con=engine) 
typemap = type_.set_index("typeid")["typecode"]

df = pd.read_sql_query(f"""{sql}""",con=engine)
rendict = {k:k.replace("-","") for k in df.columns if "-2" in k}
df = df.rename(columns=rendict)
df["typecode"] = df["typeid"].map(typemap)
df["typecode2"] = df["typeid2"].map(typemap)
df.to_csv(basedir / "data/2-interim/compare_datatypes.csv")

types = df["typecode2"].unique()
results = pd.DataFrame(index=types,columns=["avg_ana","avg_type","mse","rmse","rmse_rel","count"])
for tc in types:
    b1 = df["typecode2"] == tc
    b = (df["typecode"] == "ANA") & (df["typecode2"] == tc)
    #print(tc, b1.sum(), b.sum())
    df_sel = df.loc[b]
    plt.loglog(df_sel["value2"],df_sel["value"], "b.")
    plt.loglog([0,max(df_sel["value2"].max(),df_sel["value"].max())],[0,max(df_sel["value2"].max(),df_sel["value"].max())], "k-")
    plt.title(tc)
    plt.savefig(basedir / f"data/2-interim/oneone_{tc}.png")
    plt.close()
    diff = (df_sel["value2"] - df_sel["value"]).abs()
    reldiff = diff / df_sel["value"]
    mse = (diff**2).mean()
    rmse = mse**0.5
    rmse_rel = (reldiff**2).mean()**0.5
    #print(mse,rmse,rmse_rel, rmse / df_sel["value"].mean())
    results.loc[tc] = [df_sel["value"].mean(),df_sel["value2"].mean(),mse, rmse, rmse_rel, b.sum()]
results.astype(float).round(2).to_csv(basedir / "data/2-interim/compare_datatypes_results.csv")
print(results.astype(float).round(2))
session.close()

