import configparser
from pathlib import Path
import pandas as pd
import numpy as np
import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, MetaData, func
import matplotlib.pyplot as plt

# local
from orm_zoetzout import Base, Location, Dataseries, Compartment, Unit, Parameter, Type

basedir = Path(r"c:\Svnrepos\nhi-fresh-salt")
chloridedbdir = basedir / "src/1-prepare/chloride_database"

"""some functions"""
def read_config(af):
    # Default config file (relative path, does not work on production, weird)
    # Parse and load
    cf = configparser.ConfigParser()
    cf.read(af)
    return cf

# setup connection to database
local = False
if local:
    cf = read_config(chloridedbdir / "credentials_zoetzout_local.cfg")
else:
    cf = read_config(chloridedbdir / "credentials_zoetzout_remote.cfg")
connstr = (
    "postgres://"
    + cf.get("Postgis", "user")
    + ":"
    + cf.get("Postgis", "pwd")
    + "@"
    + cf.get("Postgis", "host")
    + ":5432/"
    + cf.get("Postgis", "dbname")
)
engine = create_engine(connstr, echo=True)

meta = MetaData(engine)

# create session --> all initialisation work has been done in orm_initialize
Session = sessionmaker(bind=engine)
session = Session()

sql = """
select l.locationcode, l.x, l.y, d.z, p.parametercode, d.value
from 
dataseries d
join location l on l.locationid = d.locationid
join parameter p on p.parameterid = d.parameterid
where p.parameterid > 4
--limit 1000
"""

df = pd.read_sql_query(f"""{sql}""",con=engine)
df["x"] = df["x"].astype(int)
df["y"] = df["y"].astype(int)
df["value"] = (df["value"] * 1000).astype(int)
df = df.set_index(["locationcode","x","y","z"])
pt = df.pivot(columns="parametercode")
pt.to_csv(basedir / "data/2-interim/freshem_percentiles.csv")