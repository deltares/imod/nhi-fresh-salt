import imod
import xarray as xr
import scipy.ndimage.morphology
from pathlib import Path
import gc

def mask_offshore(is_land, buffer_distance):
    distance = xr.full_like(
        is_land,
        scipy.ndimage.morphology.distance_transform_edt(
            ~is_land.values, sampling=250.0
        ),
        dtype=np.float,
    )
    mask = distance > buffer_distance
    return mask
    
regis_path = Path(r"data/1-external/REGIS_v2_2/REGIS_v2_2.nc")

regis = xr.open_dataset(regis_path,chunks={"formation":1,"x":2800,"y":3250})
has_data = regis["top"].max(dim="formation")

for prm in ["kv"]: #"top","bot","kh",
    tmp = imod.prepare.fill(regis[prm].load(), by="formation")
    tmp.to_netcdf(f"temp_{prm}.nc")
    del tmp
    gc.collect()

ext = xr.Dataset()
for prm in ["top","bot","kh","kv"]: #"top","bot","kh",
    ext[prm] = xr.open_dataset(f"temp_{prm}.nc")[prm]

ext.to_netcdf(regis_path.with_name(f"{regis_path.stem}_extended.nc"))