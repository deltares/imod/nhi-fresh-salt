# -*- coding: utf-8 -*-
"""
Read VES data from Dinoloket

TODO: store in database
"""
from pathlib import Path
from readers import read_VES
import pandas as pd
import datetime
import matplotlib.pyplot as plt

#VESpath = Path("p:/11203718-007-nhi-zoetzout/Gegevens_Dinoloket/Vertical electrical sounding(s) (VES)")
#result_path = Path("../../data/2-interim")
#fig_path = Path("../../reports/figures")
VESpath = Path("c:/projects/nhi-fresh-salt/data/1-external/Gegevens_Dinoloket/Vertical electrical sounding(s) (VES)")
result_path = Path("data/2-interim")
fig_path = Path("reports/figures")
#"""
attrs_l = []
meas_data_l = []
interp_data_l = []
i=0
for fn in VESpath.glob("*.txt"):
    i += 1
    print(fn.name)
    attrs,meas_data,interp_data = read_VES(fn)
    meas_data.loc[:,"VESid"] = attrs["VESid"]
    attrs_l += [attrs]
    meas_data_l += [meas_data]
    if interp_data is not None:
        interp_data.loc[:,"VESid"] = attrs["VESid"]
        interp_data_l += [interp_data]

#    if i == 50:
#        break

attrs = pd.concat(attrs_l,axis=1).T
meas_data = pd.concat(meas_data_l,axis=0,ignore_index=True)
interp_data = pd.concat(interp_data_l,axis=0,ignore_index=True)

#attrs.to_csv(result_path / "VES-attributes.csv")
#meas_data.to_csv(result_path / "VES-measurement-data.csv")
#interp_data.to_csv(result_path / "VES-interpreted-data.csv")

#Choosing only the "good" or "regular" interpretations
attrs_id = []
attrs_clean = pd.DataFrame([],index=list(attrs.columns))
interp_clean = pd.DataFrame([],index=list(interp_data.columns))
meas_clean = pd.DataFrame([],index=list(meas_data.columns))
for key, value in attrs.iterrows():
    if value['quality_interpretation'] == 'REDELIJK' or value['quality_interpretation'] == 'GOED':
        attrs_clean = pd.concat([attrs_clean,value],axis=1)
        attrs_id.append(value['VESid'])
        print(value['VESid'])
for i, attr in interp_data.iterrows():
    for j, attr_id in enumerate (attrs_id):
        if attr['VESid'] == attr_id:
            interp_clean = pd.concat([interp_clean,attr],axis=1)
            print(str(attr['VESid'])+'_interp')
for i, attr in meas_data.iterrows():
    for j, attr_id in enumerate (attrs_id):
        if attr['VESid'] == attr_id:
            meas_clean = pd.concat([meas_clean,attr],axis=1)  
            print(str(attr['VESid'])+'_meas')

attrs_clean.transpose().to_csv(result_path / "VES-attributes_clean.csv")
attrs_clean.transpose().to_pickle(result_path / "VES-attributes_clean.pickle")
meas_clean.transpose().to_csv(result_path / "VES-measurement-data_clean.csv")
meas_clean.transpose().to_pickle(result_path / "VES-measurement-data_clean.pickle")
interp_clean.transpose().to_csv(result_path / "VES-interpreted-data_clean.csv")
interp_clean.transpose().to_pickle(result_path / "VES-interpreted-data_clean.pickle")

#print(attrs.info())
#print(meas_data.info())
#print(interp_data.info())
#"""

#attrs = pd.read_pickle(result_path / "VES-attributes.pickle")
#attrs.loc[:,"year"] = attrs["date"].apply(lambda x:datetime.datetime.strptime(str(x),"%Y-%m-%d %H:%M:%S").year)
#attrs.loc[attrs["has_interpretation"]].hist(column="year",bins=40)
#plt.savefig(fig_path / "VES_distribution_years.png")

