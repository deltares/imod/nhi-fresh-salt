"""
Script to resample FRESHEM data of Zeeland and Dunea to 250m * 1m resolution
"""

import pandas as pd
import xarray as xr
#import imod
from pathlib import Path

basepath = Path(r"c:\Svnrepos\nhi-fresh-salt\data")
data_paths = {"dunea":
                {"input":basepath / r"1-external\chloride_data\dunea_freshem.csv",
                "attr":basepath / r"2-interim\dunea_freshem-attributes.pickle",
                "data":basepath / r"2-interim\dunea_freshem-measurement-data.pickle",
                },
            "zeeland":
                {"input":basepath / r"1-external\chloride_data\chloride_freshem.csv",
                "coarse":basepath / r"2-interim\chloride_freshem_250m.csv",
                "attr":basepath / r"2-interim\zeeland_freshem-attributes.pickle",
                "data":basepath / r"2-interim\zeeland_freshem-measurement-data.pickle",
                },
            "cliwat-skytem":
                {"attr":basepath / "2-interim/skytem_CliwatFryslan-attributes.pickle",
                "data":basepath / "2-interim/skytem_CliwatFryslan-data.pickle",
                "coarse-attr":basepath / "2-interim/skytem_CliwatFryslan-attributes_250m.pickle",
                "coarse-data":basepath / "2-interim/skytem_CliwatFryslan-data_250m.pickle",
                },
            "cliwat-HEM":
                {"attr":basepath / "2-interim/HEM_CliwatFryslan-attributes.pickle",
                "data":basepath / "2-interim/HEM_CliwatFryslan-data.pickle",
                "coarse-attr":basepath / "2-interim/HEM_CliwatFryslan-attributes_250m.pickle",
                "coarse-data":basepath / "2-interim/HEM_CliwatFryslan-data_250m.pickle",
                },
            }
"""
#### FIRST: DUNEA
df = pd.read_csv(data_paths["dunea"]["input"])
df = df.reindex(columns=["x","y","z","thickness","flightline","DOI"]+[f"p{p}" for p in [10,25,40,50,60,75,90]])

# unique id per location
df["locid"] = 0
df.loc[(df["x"]!=df["x"].shift(1))|(df["y"]!=df["y"].shift(1)),"locid"] = 1
df["locid"] = df["locid"].cumsum()

# add mv
grouped = df.sort_values(by="z",ascending=False).groupby(["locid"])
locs = grouped.first()
locs["mv"] = locs["z"] + 0.5 * locs["thickness"]
df["mv"] = df["locid"].map(locs["mv"])

# delete below DOI
df = df.loc[df["z"] > (df["mv"] - df["DOI"])]

# resample
locs["dist"] = ((locs["x"]-locs["x"].shift())**2+(locs["y"]-locs["y"].shift())**2)**0.5
b = locs["flightline"]!=locs["flightline"].shift(1)
locs.loc[b,"dist"] = 0
d = locs["dist"].cumsum() % 500.
b = d < d.shift(1)
b.iloc[0] = True

# resample per flightline
locs_sel = locs[b].reset_index()
df_sel = df.loc[df["locid"].isin(locs_sel["locid"])]

locs_sel.to_pickle(data_paths["dunea"]["attr"])
df_sel.reindex(columns=["locid","z"]+[f"p{p}" for p in [10,25,40,50,60,75,90]]).to_pickle(data_paths["dunea"]["data"])


#### THEN: Zeeland
d={"XG+ Gravity Center+":"x","YG+ Gravity Center+":"y","ZG+ Gravity Center+":"z"}
d.update({f"chloride{{{p}}}":f"p{p}" for p in [10,25,40,50,60,75,90]})


result = []
for chunk in pd.read_csv(data_paths["zeeland"]["input"], chunksize=100000):
    
    # rename cols
    #chunk = chunk.rename(columns=d)
    #chunk = chunk.drop(columns="SN+ Sample Number (READONLY)+")

    # is center of 250*250 m cell?
    is_center = (chunk["XG+ Gravity Center+"]%125==0)&(chunk["YG+ Gravity Center+"]%125==0)

    # reselect
    chunk = chunk.loc[is_center]

    # and append to list
    result.append(chunk)

# concat and save
df_coarse = pd.concat(result,axis=0,ignore_index=True)
df_coarse = df_coarse.rename(columns=d)
df_coarse = df_coarse.drop(columns="SN+ Sample Number (READONLY)+")

#df_coarse.to_csv(data_paths["zeeland"]["coarse"])

df_coarse = pd.read_csv(data_paths["zeeland"]["coarse"],index_col=0)

# unique id per location
df_coarse["locid"] = 0
df_coarse.loc[(df_coarse["x"]!=df_coarse["x"].shift(1))|(df_coarse["y"]!=df_coarse["y"].shift(1)),"locid"] = 1
df_coarse["locid"] = df_coarse["locid"].cumsum()

grouped = df_coarse.groupby(["locid"])
locations = grouped.first().reset_index()
locations = locations.reindex(columns=["locid","x","y"])
locations.to_pickle(data_paths["zeeland"]["attr"])

df_coarse.reindex(columns=["locid","z"]+[f"p{p}" for p in [10,25,40,50,60,75,90]]).to_pickle(data_paths["zeeland"]["data"])
"""

#### CLIWAT

for datatype in ["cliwat-skytem","cliwat-HEM"]:
    data_paths_ = data_paths[datatype]
    locs = pd.read_pickle(data_paths_["attr"])
    data = pd.read_pickle(data_paths_["data"])
    
    # resample, only every 250m
    locs["dist"] = ((locs["x"]-locs["x"].shift())**2+(locs["y"]-locs["y"].shift())**2)**0.5
    locs.loc[0,"dist"] = 0
    # if flightline available:
    if "LINE" in locs:
        b = locs["LINE"]!=locs["LINE"].shift(1)
        locs.loc[b,"dist"] = 0


    d = locs["dist"].cumsum() % 250.
    b = d < d.shift(1)
    b.iloc[0] = True
    attr_coarse = locs[b].reset_index(drop=True)
    
    # select data of coarse set
    data_coarse = data.loc[data["RECORD"].isin(attr_coarse["RECORD"])]

    attr_coarse.to_pickle(data_paths_["coarse-attr"])
    data_coarse.to_pickle(data_paths_["coarse-data"])

    #break