create or replace view open_sw_measurement_values as

select 
	vals.typecode, 
	loc.locationcode, 
	loc.owner, 
	loc.x, 
	loc.y, 
	vals.value, 
	vals.unit, 
	vals.start_time,
	vals.end_time,
	loc.geometrypoint from
(
select 
	locationid,
	typecode,
	start_time,
	end_time,
	median_value as value,
	'g/L' as unit
	from xyv_analyses_sw_aggregate
) as vals
join location loc on loc.locationid = vals.locationid
where loc.confidential = False and loc.compartmentid = 1