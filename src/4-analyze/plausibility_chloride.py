"""
Duiding resultaten a.h.v. verandering zoet-brak, brak-zout grensvlak
Needed: latest imod-python from GIT (0.9.1.dev), with latest boundaries.py (imod/evaluate)
For: Transient version NHI-ZZ (2000 - 2100; autonome ontwikkeling)
"""
import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
import imod
import os

# Predefined plot windows
totaal_nl = {'name':'totaal','xmin': 10000, 'xmax': 280000, 'ymin': 350000, 'ymax': 625000}
noord_nl = {'name':'noord','xmin': 150000, 'xmax': 280000, 'ymin': 520000, 'ymax': 620000}
west_nl = {'name':'west','xmin': 80000, 'xmax': 200000, 'ymin': 470000, 'ymax': 565000} 
zuid_nl = {'name':'zuid','xmin': 10000, 'xmax': 160000, 'ymin': 355000, 'ymax': 475000} 

# Input:
os.chdir('c:/temp/NHI')
scen = 'NHI-ZZ-2.3.0'
top_NHIzz = 'data/3-input/bas/top*.idf'
bot_NHIzz = 'data/3-input/bas/bottom_l*.idf'
#con_ss_NHIzz = 'data/4-output/conc/conc_20000101_l*.idf'
con_tr_NHIzz = 'data/4-output_tr/conc/*.idf'
shp = 'data/1-external/provincie/2018-Imergis_provinciegrenzen_kustlijn.shp'
show = totaal_nl    # Pick one of the prefined plot windows (see above)

# Read concentration (merge conc first, or use imod.idf.open_subdomains)
print('Reading concentrations...')
ds = xr.Dataset()
#conc_ss = imod.idf.open(con_ss_NHIzz)
conc_tr = imod.idf.open(con_tr_NHIzz)
#ds['conc'] = xr.concat([conc_ss, conc_tr], dim='time')
ds['conc'] = xr.concat([conc_tr], dim='time')
ds['conc'].attrs["units"] = "g/L chloride"

# Get (transient) datarray of concentration, dx, and dy
dx = np.mean(ds["conc"].x[1:].values - ds["conc"].x[:-1].values)
dy = np.mean(ds["conc"].y[:-1].values - ds["conc"].y[1:].values)

# Get z and dz of dataarray; TOP's and BOT's of NHI-ZZ
print('Get z and dz of datarray...')
top = imod.idf.open(top_NHIzz)
if top.ndim == 2:
    top['layer'] = 1
bots = imod.idf.open(bot_NHIzz)
dz1 = top - bots[0]; z1 = (top + bots[0]) * 0.5
dz2 = abs(bots.diff('layer'))
z2 = (bots[:-1].drop('layer') + bots[1:].drop('layer')) * 0.5
z2['layer'] = np.arange(2,40,1); dz2['layer'] = np.arange(2,40,1)
z = xr.concat([z1, z2], dim='layer'); dz = xr.concat([dz1, dz2], dim='layer')
ds["z"] = z.transpose('layer', 'y', 'x'); 
ds["dz"] = dz.transpose('layer', 'y', 'x')

# Change in fresh / brackish / salt groundwater volume (graph: in time)
print('Change in fresh / brackish / salt groundwater volume...')
volume = np.abs(ds["dz"]) * np.abs(dx) * np.abs(dy) * 0.3
conc_nodataval = ds['conc'].fillna(-32.768)
fresh_vol = np.logical_and(conc_nodataval >= 0., conc_nodataval < 1.) * volume
ds["groei in volume zoet grondwater"] = fresh_vol.diff("time") / 5
ds["absolute verandering in het volume zoet grondwater"] = np.abs(fresh_vol.diff("time") / 5)
ds["totale groei in het volume zoet grondwater"] = fresh_vol[1:] - fresh_vol[0]
brack_vol = np.logical_and(conc_nodataval >= 1., conc_nodataval < 8.) * volume
ds["groei in volume brak grondwater"] = brack_vol.diff("time") / 5
ds["absolute verandering in het volume brak grondwater"] = np.abs(brack_vol.diff("time") / 5)
ds["totale groei in het volume brak grondwater"] = brack_vol[1:] - brack_vol[0]
saline_vol = (conc_nodataval >= 8.) * volume
ds["groei in volume zout grondwater"] = saline_vol.diff("time") / 5
ds["absolute verandering in het volume zout grondwater"] = np.abs(saline_vol.diff("time") / 5)
ds["totale groei in het volume zout grondwater"] = saline_vol[1:] - saline_vol[0]
dsv = ds.sum("layer").sum("x").sum("y").drop("conc")
df = dsv.to_dataframe()
if not os.path.exists(f"data/5-visualization/{scen}/volume"):
    os.makedirs(f"data/5-visualization/{scen}/volume")
df.to_csv(f"data/5-visualization/{scen}/volume/changesalinity.csv")

# Plot graph with (total) change in in fresh / brackisch / saline gw volume
dfv = df[1:].drop(['dy', 'dx', 'z', 'dz'], axis=1)
dfv = dfv / 1e9; dfv['tijd'] = np.linspace(2005,2100,20)
for vol in ["groei in volume zoet grondwater", 
            "absolute verandering in het volume zoet grondwater",
            "totale groei in het volume zoet grondwater", 
            "groei in volume brak grondwater",
            "absolute verandering in het volume brak grondwater", 
            "totale groei in het volume brak grondwater",
            "groei in volume zout grondwater",
            "absolute verandering in het volume zout grondwater",
            "totale groei in het volume zout grondwater"]:
    fig = dfv.plot.scatter(x='tijd',y=vol).get_figure()
    if "total" in vol:
        plt.ylabel(f'Volume in miljard m$^{3}$')
    else:
        plt.ylabel(f'Volume in miljard m$^{3}$ jr$^{-1}$')
    fig.suptitle(vol)
    fig.align_xlabels()
    fig.tight_layout(rect=(0, 0, 1, 0.95))
    fig.savefig(f"data/5-visualization/{scen}/volume/{vol}.png", dpi=200)
    plt.close()
print(f"See csv and figures in data/5-visualization/{scen}/volume")

# Plot growth rate of fresh groundwater (as a map)
map_shp = gpd.read_file(shp)
freshgw2d = ds["groei in volume zoet grondwater"].sum(dim='layer')
freshgw2d = freshgw2d / abs(freshgw2d.dx * freshgw2d.dy * 0.3)
print("Plotting growth of fresh gw in m per yr...")
for time in ds["groei in volume zoet grondwater"].time[1:].values:
    year = pd.to_datetime(time).strftime("%Y"); print(f"...plotting: {year}")
    fig, ax = plt.subplots(subplot_kw={'xticks': [], 'yticks': []})
    bounds = [-10,-5,-1,-0.5,0,0.5,1,5,10]
    s = freshgw2d.sel(time=time).drop('dx').drop('dy')
    s.name = "groei in het volume zoet grondwater in m per jaar"
    img = s.plot(ax=ax, cmap='RdBu', levels=bounds)
    img.colorbar.set_ticklabels(bounds)
    map_shp.boundary.plot(ax=ax, color='darkgrey', linewidth=0.5)
    ax.set_xlim([show['xmin'], show['xmax']]); 
    ax.set_ylim([show['ymin'], show['ymax']])
    plt.xlabel(''); plt.ylabel('')
    fig.tight_layout()
    fig.savefig(f"data/5-visualization/{scen}/volume/{s.name} {year}.png", dpi=300)
    fig.clf(); plt.close()

# Preparation, defined plot windows and desired chloride levels
print('Determine change in fresh-brackish / brackish-saline boundary...')
z = ds["z"]; timesteps = len(ds["conc"].time)
z = z.assign_coords({"dz":(("layer","y","x"),ds["dz"])})
empty_array = np.empty((len(ds["conc"].time), len(ds["conc"].y), len(ds["conc"].x)))
chloride_levels = [1,3,5]
for cl in chloride_levels:
    if not os.path.exists(f"data/5-visualization/{scen}/boundary/{cl}"):
        os.makedirs(f"data/5-visualization/{scen}/boundary/{cl}")

# Determine and plot change in fresh-brackish / brackish-saline boundary
for cl in chloride_levels:
    clbnd = f"boundary {cl} g/l chloride"; print(f"...{clbnd}")
    ds[clbnd] = xr.DataArray(empty_array, 
          dims={'time':ds["conc"].time, 'y':ds["conc"].y, 'x':ds["conc"].x})
    for t in range(timesteps):
        exceed,nonexc = imod.evaluate.interpolate_value_boundaries(ds["conc"][t],z,cl)
        ds[clbnd][t] = exceed.sel(boundary=0)

    # Plot change in boundary (each 5 years)
    ds["verandering in grensvlak"] = ds[clbnd].diff("time") / 5
    ds["totale verandering in grensvlak"] = ds[clbnd][1:] - ds[clbnd][0]   
    for bnd in ["verandering in grensvlak", "totale verandering in grensvlak"]:
        print(bnd)
        for time in ds["conc"].time[1:].values:
            year = pd.to_datetime(time).strftime("%Y")
            print(f"...plotting: {year}")
            fig, ax = plt.subplots(subplot_kw={'xticks': [], 'yticks': []})
            s = ds[bnd].sel(time=time).drop('dx').drop('dy') # reverse
            if "total" in bnd:
                s = s.rename('Totale verschuiving (omhoog) in m')
                bounds = [-50,-10,-5,-1,0,1,5,10,50]
            else:
                s = s.rename('Verschuiving (omhoog) in m jr$^{-1}$')
                bounds = [-10,-5,-1,-0.5,0,0.5,1,5,10]
            img = s.plot(ax=ax, cmap='RdBu_r', levels=bounds)
            img.colorbar.set_ticklabels(bounds)
            map_shp.boundary.plot(ax=ax, color='darkgrey', linewidth=0.5)
            ax.set_xlim([show['xmin'], show['xmax']]); 
            ax.set_ylim([show['ymin'], show['ymax']])
            plt.xlabel(''); plt.ylabel('')
            figname = f"{bnd} {cl} gl chloride in {year}.png"
            fig.tight_layout()
            fig.savefig(f"data/5-visualization/{scen}/boundary/{cl}/{figname}",dpi=300)
            fig.clf()
            plt.close()

#ds.to_zarr("data/5-visualization/fbs_volume_analysis.zarr")
print(f'Succesfull, see files in data/5-visualization/{scen}!')