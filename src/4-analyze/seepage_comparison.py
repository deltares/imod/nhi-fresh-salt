import xarray as xr
import os
import imod
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colorbar
import geopandas as gpd
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# LHMzz version
version = "v2.3.0"

# open paths
basepath = Path(".")
#path_layermodel = basepath / "data/2-interim/layermodel.nc"
path_flf = basepath / f"data/4-output/{version}/bdgflf/bdgflf*.idf"
path_drn = basepath / f"data/4-output/{version}/bdgdrn/bdgdrn*.idf"
path_flf_lhm = basepath / "data/1-external/LHM4.0_steady_state_v3/bdgflf/bdgflf_STEADY-STATE_l1.idf"
path_riv_lhm = basepath / "data/1-external/LHM4.0_steady_state_v3/bdgriv/bdgriv_sys6_STEADY-STATE_l2.idf"
path_landen = basepath / "data/1-external/shapes/Landen.shp"
path_prov = basepath / "data/1-external/shapes/provincie.shp"
path_resleg = basepath / "data/1-external/legends/residuals.leg"
result_path = basepath / f"data/5-visualization/seepage_comparison_{version}"
result_path.mkdir(exist_ok=True)

# load
#layermodel = xr.open_dataset(path_layermodel)
flf = imod.idf.open(path_flf).isel(time=0).sel(layer=10)  # positive upwards
drn = imod.idf.open(path_drn).isel(time=0).sel(layer=[11,12,13,14,15]).sum(dim="layer")  # negative
flf_lhm = imod.idf.open(path_flf_lhm).squeeze(drop=True)
riv_lhm = imod.idf.open(path_riv_lhm).squeeze(drop=True)
landen = gpd.read_file(path_landen)
landen_no_NL = landen[landen["CNTRYABB"] != "NL"]
provincie = gpd.read_file(path_prov)

# seepage = FLF + DRN, or FLF+RIV for LHM
seep = flf.where(flf > 0, 0) - drn
seep_LHM = flf_lhm.where(flf_lhm > 0, 0) - riv_lhm
diff = seep - seep_LHM
diffrel = diff / seep_LHM * 100. # pct

# to mm/d  m3/d -> /250/250*1000
diff = diff / 250 / 250 * 1000

colors,levels = imod.visualize.read_imod_legend(path_resleg)
overlays = [{"gdf": landen_no_NL, "color": "white", "edgecolor": "white"}, {"gdf": provincie, "color":"None", "edgecolor": "k"}]
fig, ax = imod.visualize.plot_map(
    diff, levels=levels, colors=colors, figsize=(10, 10), overlays=overlays, kwargs_colorbar={"label":"Verschil kwelflux (mm/d)"}
)
_ = ax.set_xticklabels([])
_ = ax.set_yticklabels([])
#ax.set_title(f"Verschil kwelflux LHM 4.0 - LHMzz {version} laag 1")
fig.savefig(result_path / f"LHMv4_min_LHMzzv{version}_l1.png", bbox_inches="tight")


# relative change, exclude kwelflux diff < 0.05 mm/d
diffrel = diffrel.where(((np.abs(diff) > 0.05) & ~(seep == 0) & ~(seep_LHM == 0)) | diff.isnull(), 0)
levels = [-100,-50,-25,-10,-1,1,10,25,50,100]
colors = "coolwarm"
fig, ax = imod.visualize.plot_map(
    diffrel, levels=levels, colors=colors, figsize=(10, 10), overlays=overlays, kwargs_colorbar={"label":"Relatief verschil kwelflux (%)"}
)
_ = ax.set_xticklabels([])
_ = ax.set_yticklabels([])
#ax.set_title(f"Verschil kwelflux LHM 4.0 - LHMzz {version} laag 1")
fig.savefig(result_path / f"LHMv4_min_LHMzzv{version}_pct_l1.png", bbox_inches="tight")