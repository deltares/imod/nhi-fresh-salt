import xarray as xr
import os
import imod
import pandas as pd
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))

# LHMzz version
version = "0.1.17"

# open paths
basepath = Path(".")
path_results = basepath / f"data/4-output/V{version}"
result_path = basepath / f"reports/waterbalance_V{version}.csv"
#result_path.mkdir(exist_ok=True)

# load
prms = ["bdgbnd","bdgdrn","bdgghb","bdgrch","bdgriv","bdgwel"]

resultdf = pd.DataFrame(index=prms, columns=["IN","OUT"])

for prm in prms:
    print (prm)

    try:
        # assume subdomains
        da = imod.idf.open_subdomains(path_results / f"{prm}/{prm}*_p*.idf")
        da = da.isel(time=0)

        # sum pos and neg
        resultdf.loc[prm,"IN"] = float(da.where(da > 0).sum())
        resultdf.loc[prm,"OUT"] = float(da.where(da < 0).sum())
    except:
        print(f"{prm} failed")
        pass

resultdf.to_csv(result_path)
