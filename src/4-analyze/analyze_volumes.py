# -*- coding: utf-8 -*-
"""
Script to analyse change in volume of fresh/brackish/saline over time
"""
import os
import xarray as xr
import imod
import pandas as pd
import matplotlib.pyplot as plt

run = "V0.1.17_tr_merged"
path_cl = f"data/4-output/{run}/conc/conc*"
path_laymodel = f"data/2-interim/layermodel_corr.nc"
path_result = f"reports/figures/volumes_{run}.png"

cl = imod.idf.open(path_cl)
lay = xr.open_dataset(path_laymodel)
lay["volume"] = 0.3 * lay["dz"] * 250. * 250.

classes = {"Fresh (< 0.15 g/L)":[0,.15],
            "Brackish (0.15 - 10 g/L)":[.15,10.],
            "Saline (> 10 g/L)":[10,9999.]}

# Ignore deeper layers
include = lay["lhmfresh_layer"] <= 11  #LHM layer <= 6
cl = cl.where(include)

df = pd.DataFrame(index=cl.coords["time"].values, columns=classes.keys())
for class_, (low,high) in classes.items():
    cl_class = ((cl >= low) & (cl < high)).astype(int)
    vol_class = cl_class * lay["volume"]
    df.loc[:,class_] = vol_class.sum(dim=["layer","y","x"]).to_series()

df.plot()
plt.title("Volume salinity classes")
plt.xlabel("time (year)")
plt.ylabel("Effective volume (m3)")
plt.savefig(path_result)

