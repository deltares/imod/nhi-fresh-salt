"""
Calculate Cl- boundaries from either original 3D chloride interpolation, or corrected LHM fresh-salt grid

For:
- p25, p50, p75
- 150 mg/L, 1000 mg/L, 5000 mg/L

"""

import imod
import os
import xarray as xr
import pandas as pd
import numpy as np
import geopandas as gpd
from rasterio.crs import CRS
from pathlib import Path
from scipy.ndimage import percentile_filter, uniform_filter, label

crs = CRS.from_epsg(28992)  # Amersfoort RD New

def cl_include_topbot(cl):
    """Function to transform 3d Kriging result to Dataarray suited
    for further processing"""
    cl = cl.rename(
        {
            "x-coordinates": "x",
            "y-coordinates": "y",
            "z-coordinates": "z",
            "Chloride dimension": "percentile",
        }
    )
    cl = cl.sortby("z", ascending=False)  # flip z
    z = cl.z.to_series().astype(float)
    z = z.reset_index(drop=True)
    top = z.rolling(2).mean()
    bot = top.shift(-1)
    top.iloc[0] = z.iloc[0] + (z.iloc[0] - bot.iloc[0])
    bot.iloc[-1] = z.iloc[-1] - (top.iloc[-1] - z.iloc[-1])
    dz = top - bot
    z = 0.5 * top + 0.5 * bot  # recalculate z due to differences in dz

    cl.coords["z"] = range(1, len(z) + 1)
    cl.coords["percentile"] = ["p25", "p50", "p75"]
    cl = cl.rename({"z": "layer"})
    cl = cl.transpose("percentile", "layer", "y", "x")

    # to DataArray suited for idf save
    dims = ("percentile", "layer", "y", "x")
    coords = {
        "percentile": cl.percentile.values,
        "layer": cl.layer.values,
        "y": cl.y.values.astype(float),
        "x": cl.x.values.astype(float),
        "z": ("layer", z),
        "dz": ("layer", dz),
        "top": ("layer", top),
        "bottom": ("layer", bot),
    }
    cl2 = xr.DataArray(cl.values, coords, dims, name="3d-chloride")
    cl2 = cl2.sortby("y", ascending=False)  # flip y

    # return dataset
    return cl2

# run from basedir, assuming script resides in subdir of src/
os.chdir(os.path.join(os.path.dirname(__file__), "..", ".."))


# Paths
path_layermodel = "data/2-interim/layermodel.nc"
path_3dkrig = "p:/11203718-007-nhi-zoetzout/3D-chloride/run5/Chloride_normlog_25_50_75.nc"
path_sconc = "data/2-interim/starting_concentration.nc"
path_clresult = f"data/4-output/{version}/conc/conc*.idf"
path_boundaries = f"reports/figures/grensvlakken_NGR/grensvlakken.nc"
path_bnd = "data/2-interim/bnd.nc"
path_template_2d = "data/2-interim/template_2d.nc"
path_landen = "data/1-external/shapes/landen.shp"
path_results = Path("data/5-visualization/grensvlakken_NGR")

path_results.mkdir(exist_ok=True, parents=True)

# Open LHMzz layermodel
layermodel = xr.open_dataset(path_layermodel)
new_top = layermodel["top"]
new_bot = layermodel["bot"]
bnd = xr.open_dataset(path_bnd)["ibound"]

# Open 3D chloride result and transform to include top/bot info
conc3d = xr.open_dataarray(path_3dkrig) / 1000.  # to g/L
conc3d = cl_include_topbot(conc3d)
# cut off above surface level 
toplay = imod.select.upper_active_layer(new_top,False)
top_lm = new_top.where(new_top.layer == toplay).min(dim="layer")
conc3d = conc3d.where(~top_lm.isnull()).where(conc3d.bottom <= top_lm)

# For dataportal: cut off below GHB and assign attrs
conc3d_dp = conc3d.where(conc3d.top > new_bot.min(dim="layer"))
conc3d_dp.attrs["crs"] = "EPSG:28992"
conc3d_dp.attrs["parameter"] = "groundwater chloride concentration"
conc3d_dp.attrs["unit"] = "g/L"
#conc3d_dp.to_netcdf(path_dataportal / "3dchloride_groundwater_gL.nc") # save intermediate result for dataportaal

landen = gpd.read_file(path_landen)
nlmask = landen.loc[landen.CNTRYABB=="NL"]
nlmask.crs = crs

ghb = new_bot.min(dim="layer")

exc_all = []
for clb in [0.15, 1., 5.]:
    excs_pct = []
    for pct in ["p25","p50","p75"]:
        # Create grid of depth of fresh/brackish interface
        exc, _ = imod.evaluate.interpolate_value_boundaries(conc3d.sel(percentile=pct), conc3d.z, clb)
        exc1 = exc.max(dim="boundary").fillna(ghb)
        exc1 = exc1.where(exc1 > ghb, ghb)  # otherwise goes below GHB
        excs_pct.append(exc1.assign_coords(percentile=pct, bndvalue=clb))
    exc_all.append(xr.concat(excs_pct, dim="percentile"))
exc_all = xr.concat(exc_all, dim="bndvalue")

# clip to NL
exc_all = exc_all.rio.write_crs(crs)
exc_all = exc_all.rio.clip(nlmask)

exc_all.to_netcdf(path_results / "grensvlakken_NGR.nc")
imod.idf.save(path_results / "grensvlak", exc_all, pattern="{name}_{bndvalue}gL_{percentile}.idf")
imod.rasterio.save(path_results / "grensvlak", exc_all, pattern="{name}_{bndvalue}gL_{percentile}.tif")
imod.idf.save(path_results / "ghb_lhm", ghb)
imod.rasterio.save(path_results / "ghb_lhm.tif", ghb)